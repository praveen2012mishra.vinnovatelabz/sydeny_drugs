#!/bin/bash

PORT=9337

#Get process_id of running server
stopRunningServer(){
echo "Get process_id of running server"

OUTPUT=$(netstat -nlp | grep ${PORT} | awk "{print \$7}")

for var in $OUTPUT

do
     PROCESS_ID="$(echo $var | cut -d'/' -f1)"

echo "Running process id = $PROCESS_ID"

#Kill the running process
kill -9 $PROCESS_ID
done

# Wait for 2 seconds
sleep 2

}


#Move new files to project directory
deployNewChanges(){
echo "Move new files to project directory"

#Remove all files except node_modules
rm -r `find . | grep -v "node_modules" | grep -v "remote_deploy.sh" | grep -v "start-server.sh"`

#Extract tar file and install npm
tar -xvf ../temp_rx_sydneydrugs/deploy.tar -C .
npm install

# Wait for 2 seconds
sleep 2

}

#Clean the temp files
cleanTempFiles(){
echo "Cleaning temp files"

rm -rf ../temp_rx_sydneydrugs

# Wait for 2 seconds
sleep 2

}


#Start server
startServer(){
echo "Start server"

DIR=$(pwd)

echo "Starting server from location : ${DIR}"

./start-server.sh

}

stopRunningServer
deployNewChanges
cleanTempFiles
#Commented start server script as it was not creating nohup output file
#startServer

echo "Server started successfully and running on port ${PORT}"



