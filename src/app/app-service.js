import AuthService from './components/auth/AuthService';

export function getAuth() {
    return (fetch('/public/clientConfig', {
        method: "GET"
    }).then((response) => response.json()).then((response) =>{
        return new AuthService(response.auth0);
    }));
}

export function getIntercomConfig() {
    return (fetch('/api/util/iConfig', {method: "GET"}).then((response) => {
        return response.json();
    }));
}