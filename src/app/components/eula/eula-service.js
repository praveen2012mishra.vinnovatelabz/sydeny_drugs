
export function acceptTermsAndConditions(){
    return(fetch('/api/util/acceptTermsAndConditions',{method:"GET"}).then((response)=>{
        return response.json();
    }));
}