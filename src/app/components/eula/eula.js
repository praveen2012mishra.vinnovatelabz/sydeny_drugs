import React,{Component} from 'react';
import {
    Modal,
    ModalHeader,
    ModalTitle,
    ModalBody,
    ModalFooter
} from 'react-modal-bootstrap';
import history from '../history';
import { acceptTermsAndConditions } from './eula-service';

import './eula.scss';

export default class Eula extends Component{

    constructor(props){
        super(props);
        this.acceptTerms = this.acceptTerms.bind(this);
    }

    acceptTerms(){
        acceptTermsAndConditions().then((response)=>{
          if(response){
              history.push('/app/dashboard');
          }
        });
    }

    render(){
        return(<div className="eula container-fluid">
            <Modal isOpen={true} size="modal-lg">
                <ModalHeader>

                    <ModalTitle>Terms and Conditions</ModalTitle>
                </ModalHeader>
                <ModalBody>
                        <p className="text">October 29, 2017 </p><br/>

                        <p className="text">SydneyDrugs App End User License Agreement </p><br/>
                        <p className="text">If you are experiencing a medical crisis, please call 000 or contact your local emergency
                            assistance
                            service
                            immediately. </p><br/>
                        <p className="text">BY USING THE SOFTWARE OR THE SERVICES, YOU AFFIRM THAT YOU ARE OF LEGAL AGE TO ENTER INTO
                            THIS EULA.</p>
                        <br/>
                        <p className="text"> This End User License Agreement (“EULA”) is an agreement between Cloud Pharmacy Services
                            Pty Ltd ABN
                            57600839834 (“SydneyDrugs”, “we,” or “us”) and you. This EULA Applies to your use of the SydneyDrugs
                            App,
                            webapp and website (the “Software”) and any information, services, or functionality provided by or in
                            connection with the Software, and any documentation or materials related to foregoing, and any services
                            or
                            goods provided by Cloud Pharmacy Services Pty Ltd or one of its subsidiaries or affiliates
                            (collectively,
                            “Services”).</p>
                        <br/>
                        <p className="text"> Please read this EULA carefully and completely before using our Software or our Services.
                            By tapping or
                            clicking “I agree,” “I accept” or any other similar button or box with respect to this EULA, or by
                            installing or using our Software or by using (including any access to) our Services, you agree to be
                            bound
                            by this EULA, which incorporates by this reference any additional terms and conditions made available to
                            you
                            by SydneyDrugs in connection with the Software or the Services.</p>
                        <br/>
                        <p className="text"> THIS EULA CONTAINS A MANDATORY ARBITRATION OF DISPUTES PROVISION THAT REQUIRES THE USE OF
                            ARBITRATION ON
                            AN
                            INDIVIDUAL BASIS TO RESOLVE DISPUTES, RATHER THAN JURY TRIALS OR CLASS ACTIONS.</p>
                        <br/>
                        <p className="text"> We reserve the right, at any time and from time to time, at our sole and absolute
                            discretion to update,
                            revise, supplement or otherwise modify this EULA. These modifications shall have immediate prospective
                            effect following our notifying you of such modifications by any reasonable means, including by making
                            available the modified EULA through the Software or the Services.</p>
                        <br/>
                        <p className="text"> INSTALLING ANY NEW VERSION OF THE SOFTWARE, OR YOUR CONTINUED USE OF THE SOFTWARE OR THE
                            SERVICES, SHALL
                            BE
                            DEEMED YOUR ACCEPTANCE OF THE MODIFIED EULA.</p>
                        <br/>
                        <p className="text"> If you do not agree to this EULA, you may not use the Software or the Services. If you do
                            not agree to
                            the
                            current version of this EULA, you are not entitled to download the Software or to use the Software or
                            the
                            Services, and you shall terminate the installation process and/or cease all use of the Software and the
                            Services; if you have already downloaded the Software despite this prohibition, you must promptly delete
                            the
                            Software from your device and cease all use of any of the Services.</p><br/>

                        <p className="heading">Limited License</p><br/>
                        <p className="text"> Subject to your compliance with this EULA, and solely for so long as you are permitted by
                            SydneyDrugs to use
                            the Software, SydneyDrugs grants to you a limited, non-transferable, non-exclusive, non-assignable,
                            revocable license (without right of sublicense) to install and use the Software on your device solely
                            for
                            your personal, non-commercial use. The Software is licensed, not sold, to you. </p><br/>

                        <p className="text"> You have no ownership rights in, or related to, the Software or the Services. As between
                            SydneyDrugs and
                            you, SydneyDrugs retains all right, title, and interest in and to the Software and the Services
                            (including
                            any changes, modifications, or corrections thereto). </p><br/>

                        <p className="heading">Medical Information and Health Content</p><br/>
                        <p className="text">OUR SOFTWARE AND OUR SERVICES CONTAIN GENERAL MEDICAL AND PHARMACEUTICAL INFORMATION FOR
                            EDUCATIONAL
                            PURPOSES ONLY; IT IS NOT A SUBSTITUTE FOR MEDICAL JUDGMENT, ADVICE, DIAGNOSIS, OR TREATMENT OF ANY
                            HEALTH
                            CONDITION OR PROBLEM.</p><br/>

                        <p className="text"> The Software and the Services do not address all possible uses, actions, precautions, side
                            effects, or
                            interactions of drugs, nor do they provide comprehensive information concerning any particular disease
                            or
                            medical condition. You should consult with a professional health care provider prior to making any
                            decisions, or undertaking any actions or not undertaking any actions related to any health care problem
                            or
                            issue. If you have any questions about the risks or benefits of taking a particular drug or about a
                            specific
                            health condition, you should consult a licensed practitioner. You should never disregard, avoid, or
                            delay
                            obtaining medical advice from a licensed practitioner because of information in the Software or the
                            Services. </p><br/>

                        <p className="text"> Proper treatment of health conditions depends upon a number of factors, including among
                            other things, your
                            medical history, diet, lifestyle, and medication regimen. Your health care provider can best assess and
                            address individual health care needs. You should consult with your health care provider before starting
                            a
                            new diet, fitness, or supplement regimen. You should also check product information (including package
                            inserts) regarding dosage, precautions, warnings, interactions, and contraindications before
                            administering
                            or using any device, drug, herb, vitamin, or supplement discussed in the Software or the Services.
                            SydneyDrugs does not endorse manufacturers’ or others’ claims about the efficacy of these or any other
                            products. </p><br/>

                        <p className="text"> Reminders in the Software or the Services to take medication reflect merely an automated
                            effort to provide
                            notice, but this may not reflect current health care directions from your health care professional.
                            REMINDERS SHOULD NOT BE TAKEN AS MEDICAL ADVICE AND MAY NOT REFLECT ANY CHANGES IN TREATMENT DIRECTIONS
                            AND
                            MAY FAIL TO BE DELIVERED IF YOUR COMPUTING DEVICE OR THE SOFTWARE IS NOT FUNCTIONING AS PLANNED. IT IS
                            IMPORTANT TO MAINTAIN A NON-ELECTRONIC REMINDER TO TAKE YOUR MEDICATION AS CURRENTLY DIRECTED IN THE
                            EVENT
                            YOUR DEVICE FAILS. </p><br/>

                        <p className="text"> SYDNEYDRUGS DOES NOT ACT IN RESPONSE TO YOUR MEDICATION REMINDER NOTICES OR YOUR RESPONSE
                            OR LACK OF
                            RESPONSE TO THESE NOTICES OR OTHERWISE PROVIDE HEALTH CARE ADVICE THROUGH THE SOFTWARE OR THE SERVICES.
                            INFORMATION RECEIVED VIA THE SOFTWARE OR THE SERVICES SHOULD NOT BE RELIED UPON FOR PERSONAL, MEDICAL,
                            LEGAL, OR FINANCIAL DECISIONS. YOU SHOULD CONSULT AN APPROPRIATE PROFESSIONAL FOR SPECIFIC ADVICE
                            TAILORED
                            TO YOUR SITUATION. THE INFORMATION PROVIDED IN THE SOFTWARE AND THE SERVICES DOES NOT CONSTITUTE
                            FINANCIAL,
                            LEGAL, OR MEDICAL ADVICE. NO REPRESENTATION IS MADE REGARDING THE ACCURACY OR IMPORTANCE OF ANY
                            INFORMATION
                            REFERENCED HEREIN. </p><br/>

                        <p className="text">SYDNEYDRUGS DOES NOT PROVIDE ANY FORM OF INSURANCE.</p><br/>

                        <p className="heading">Location Based Services</p><br/>
                        <p className="text"> By downloading the Software and enabling your Bluetooth capabilities and/or location-based
                            services on your
                            device, you expressly consent to SydneyDrugs or its suppliers or vendors collecting the precise location
                            information of your device. This information may be used in accordance with our Privacy Policy. Please
                            see
                            our Privacy Policy for further information. </p><br/>

                        <p className="heading">Australia Only Restriction</p><br/>
                        <p className="text">The Software and the Services, including the information pertaining to SydneyDrugs, are
                            highlighted for use
                            only by residents of Australia, and are not highlighted to subject SydneyDrugs to any non-Australian
                            jurisdiction or law. The Software and the Services may not comply with legal requirements of foreign
                            countries. We may limit the availability of the Software or the Services at any time, in whole or in
                            part,
                            to any person, geographic area, or jurisdiction that we choose. </p><br/>

                        <p className="heading">Usage Rules and Requirements</p><br/>
                        <p className="text"> You agree that your use of the Software and the Services shall be subject to and governed
                            by any use
                            requirements Applicable to the Software or the Services that are established from time to time by
                            SydneyDrugs or otherwise made available to you by SydneyDrugs. You agree to use the Software and the
                            Services only in accordance with these requirements. </p><br/>

                        <p className="text"> You acknowledge that the Software and the Services contain proprietary trade secrets of
                            SydneyDrugs and/or
                            its licensors and suppliers. You may not (a) submit any automated or recorded requests to the Software
                            or
                            the Services unless otherwise Approved by SydneyDrugs; (b) access the Software or the Services with
                            software
                            or other means other than the Software; (c) copy, reproduce, port, translate, modify, distribute, sell,
                            rent, lease, loan, timeshare, create derivative works based on, or otherwise exploit the Software or the
                            Services or in any other manner duplicate the Software or the Services, in whole or in part; (d)
                            decompile,
                            disassemble, reverse engineer, or otherwise attempt to derive, reconstruct, identify, or discover any
                            source
                            code, underlying ideas, or algorithms of the Software or the Services by any means, except to the extent
                            the
                            foregoing restriction is prohibited by Applicable law; (e) remove any proprietary notices, labels, or
                            marks
                            from the Software or the Services; (f) use the Software or the Services for purposes of comparison with
                            or
                            benchmarking against products or services made available by third parties; or (g) knowingly take any
                            action
                            that would cause the Software or the Services to be placed in the public domain. </p><br/>

                        <p className="text"> You agree to use the Software and the Services only for lawful purposes. You agree not to
                            interrupt or
                            attempt to interrupt the operation of the Software or the Services in any way. Any conduct by you that,
                            in
                            our sole discretion, restricts, inhibits, or interferes with the ability of any other user to enjoy the
                            Software or the Services (including by means of hacking or defacing any portion of the Software or the
                            Services, or by engaging in spamming, flooding, or other disruptive activities, including with respect
                            to
                            the servers or networks used to make the Software and the Services available) will not be tolerated. You
                            are
                            strictly prohibited from communicating on or through the Software or the Services any unlawful, harmful,
                            offensive, threatening, abusive, harassing, defamatory, vulgar, obscene, profane, hateful, fraudulent,
                            sexually explicit, racially, ethnically, or otherwise objectionable material of any sort, including, but
                            not
                            limited to, any material that encourages conduct that would constitute a criminal offense, give rise to
                            civil liability, or otherwise violate any Applicable local, state, national, or international law. </p>
                        <br/>

                        <p className="text"> You must not (a) transmit or otherwise make available through or in connection with the
                            Software or the
                            Services any virus, worm, Trojan horse, spyware, or other computer code, file, or program that is or is
                            potentially harmful or invasive or highlighted to damage or hijack the operation of, or to monitor the
                            use
                            of, any hardware, software, or equipment; (b) harvest or collect information about users of the Software
                            or
                            the Services; or (c) systematically download and store Services content. </p><br/>

                        <p className="text"> We reserve the right to terminate or suspend your use of (including any access to) the
                            Software or the
                            Services without notice, if we believe, in our sole discretion, that your actions are in violation of
                            this
                            EULA or any Applicable law, or are harmful to our interests or the interests, including intellectual
                            property or other rights, of another user or any other third party, including any of our partners,
                            affiliates, sponsors, providers, licensors, or merchants. </p><br/>

                        <p className="text"> With respect to any information you provide, you agree to provide true, accurate, current,
                            and complete
                            information, including information about yourself. It is your responsibility to maintain and promptly
                            update
                            this account information to keep such information true, accurate, current, and complete. If you provide
                            any
                            information that is fraudulent, untrue, inaccurate, incomplete, or not current, or if we have reasonable
                            grounds to suspect that such information is fraudulent, untrue, inaccurate, incomplete, or not current,
                            we
                            reserve the right to suspend or terminate your account without notice and refuse any and all current and
                            future use of the Software or the Services. </p><br/>

                        <p className="text"> Because any termination of your access to the Software or the Services may be effected
                            without prior notice,
                            you acknowledge and agree that we may immediately deactivate or delete your account and all related
                            information and files in your account and bar any further access to such files or the Software or the
                            Services. </p><br/>

                        <p className="text"> Furthermore, you agree that we shall not be liable to you or any third party for any
                            termination of your
                            access to your account or the Software or the Services. </p><br/>

                        <p className="text"> In connection with creating or maintaining an account, you may be asked to supply a user
                            name and password,
                            or a personal identification number (PIN) or other access credential (collectively, “Access
                            Credentials”) in
                            order to use the Software and/or any or all of the Services. </p><br/>

                        <p className="text"> Further, the Software may use or permit the use of authentication technologies, such as
                            Apple’s Touch ID
                            technology (“Authentication Technologies”), in connection with your use of the Software and/or any or
                            all of
                            the Services, including in connection with your Access Credentials. </p><br/>

                        <p className="text"> YOU ACKNOWLEDGE AND AGREE THAT, IF THE SOFTWARE USES OR PERMITS USE OF TOUCH ID OR OTHER
                            BIOMETRIC-BASED
                            AUTHENTICATION TECHNOLOGY, ANYONE WITH A RELEVANT BIOMETRIC IDENTIFIER (SUCH AS A FINGERPRINT, IN THE
                            CASE
                            OF TOUCH ID) STORED ON YOUR DEVICE MAY HAVE ACCESS TO YOUR ACCOUNT AND BE MAY BE ABLE TO USE THE
                            SOFTWARE
                            AND/OR ANY OR ALL OF THE SERVICES (INCLUDING TO SET OR DELETE REMINDERS, ACCESS YOUR PRESCRIPTION
                            INFORMATION, MAKE OR ENGAGE IN TRANSACTIONS, AND TO OTHERWISE AUTHORIZE PAYMENTS). </p><br/>

                        <p className="text"> You are responsible for maintaining the confidentiality of your Access Credentials and are
                            fully responsible
                            for all activities that occur in connection with your Access Credentials. You agree to immediately
                            notify us
                            of any unauthorized use of your Access Credentials or any other breach of security. You further agree
                            that
                            you will not permit others, including those whose accounts have been terminated, to access the Software
                            or
                            the Services using your Access Credentials. </p><br/>

                        <p className="text"> We may refuse to grant you an Access Credential requested by you that impersonates someone
                            else, is or may
                            be illegal, is or may be protected by trademark or other proprietary rights law, is vulgar or otherwise
                            offensive, or may cause confusion, as determined by us in our sole discretion. For security reasons,
                            Access
                            Credentials must be non-obvious, hard-to-guess, confidential, and changed on a regular basis, and you
                            must
                            log out at the end of each session. All Access Credentials remain the property of SydneyDrugs, and may
                            be
                            cancelled or suspended at any time by SydneyDrugs without any prior notice or any liability to you or
                            any
                            other person. SydneyDrugs is not under any obligation to verify the actual identity or authority of the
                            user
                            of any Access Credential. If SydneyDrugs, in its sole discretion, considers an Access Credential to be
                            insecure, then SydneyDrugs may cancel such Access Credential. </p><br/><br/>

                        <p className="text">Installation, Updates and Maintenance </p><br/>

                        <p className="text"> You acknowledge and agree that: </p><br/>

                        <p className="text"> The App requires a compatible 3G or higher enabled mobile device with internet connectivity
                            and access to
                            the Android Market site or Google Play™ operated by Google Inc. or the Mac Apple App Store™ and Apple
                            App
                            Store™ operated by Apple; </p><br/>

                        <p className="text"> Your use of the App may incur internet data charges, may involve the downloading of images,
                            content and
                            other items that may attract internet data fees and will consume the battery charge of Your mobile
                            device; </p> <br/>
                        <p className="text">The App does not provide for international roaming, and you may incur international roaming
                            charges if the
                            App or Your mobile device is used outside Australia; </p><br/>
                        <p className="text"> The continued availability of the App, and Content provided through the operation and use
                            of the App, may be
                            subject to external factors out of Our control including but not limited to routine maintenance,
                            malfunction
                            in equipment, hardware or software, internet access, and delay or failure of transmission; </p><br/>
                        <p className="text"> We may modify or update the App at any time without notice including but not limited to
                            disabling or
                            enabling certain features or functionalities of the App, introducing new features and functionalities to
                            the
                            App, bug fixes, error corrections, and workflow and design changes; </p><br/>
                        <p className="text"> We may, but We are not obligated to, provide maintenance and technical support for the App
                            from time to time
                            and we may suspend, terminate or disable some or all of the features and functionalities of the App in
                            order
                            to provide such services (if any) at any time without notice; </p><br/>
                        <p className="text"> As part of the App installation process your mobile device settings may change and you
                            acknowledge that by
                            installing the App you Approve any such changes to your mobile device settings; </p><br/>
                        <p className="text"> You may be required to download the latest version of the App from time to time including
                            but not limited to
                            any updates. It is your responsibility to ensure that you are using the most up to date version of the
                            App
                            at any given time. </p><br/>
                        <p className="heading">Content, Copyrights, and Trademarks</p><br/>
                        <p className="text">The Software and any related documentation including the content made available by
                            SydneyDrugs through the
                            Software or the Services (collectively, the “Content”) is protected under Australian and international
                            copyright laws, is subject to other intellectual property and proprietary rights and laws, and is owned
                            by
                            SydneyDrugs and/or its licensors and suppliers. Any copyright or trademark notices may not be deleted or
                            altered in any way. Our copyrights and trademarks may not be used in connection with any products or
                            services that are not ours, or in any manner that is likely to cause confusion or otherwise violate our
                            rights.</p><br/>
                        <p className="text"> As between SydneyDrugs and you, SydneyDrugs owns all the Content, including text, graphics,
                            legends,
                            customized graphics, original photographs, data, images, music, audio and video clips, typefaces,
                            titles,
                            button icons, logos, designs, words or phrases, page headers, and software as well as the design,
                            coordination, arrangement, enhancement, and presentation of this material. Copying, publishing,
                            broadcasting, re-broadcasting, webcasting, transmitting, modifying, deleting, augmenting, distributing,
                            downloading, storing, reproducing, sublicensing, adapting, creating derivative works of this Content, or
                            posting or otherwise making available this Content (including selected portions of this Content) in any
                            manner on any network computer, broadcast media, or other technologies existing now or hereinafter
                            developed
                            for any purpose other than for your personal, non-commercial use is strictly prohibited absent the prior
                            written consent of SydneyDrugs. Except as expressly provided herein, you may not copy, modify, publish,
                            transmit, sell, offer for sale, or redistribute the Content in any way without the prior written
                            permission
                            of SydneyDrugs. You hereby agree not to reproduce, duplicate, copy, sell, resell, decompile,
                            disassemble, or
                            exploit for any commercial purposes any portion of the Software, the Services, or Content, or to collect
                            any
                            information about Software or Services users. No title, rights, or interests in any content or other
                            materials made available via the Software or the Services are afforded to you; to the extent that you
                            are
                            authorized to download any such materials via the Software or the Services, any such downloaded
                            materials
                            shall be for your own personal, noncommercial use only. You may not assign this EULA or any of the
                            rights or
                            licenses granted under this EULA or rent, lease, or lend the Software or the Services to any person or
                            entity. Any attempted sublicense, transfer, or assignment in violation of this EULA is void. We may
                            assign,
                            transfer, or sublicense any or all of our rights or obligations under this EULA without restriction.</p>
                        <br/>
                        <p className="text">The product and service names and logos, slogans, trade dress, and other similar
                            designations of source or
                            origin displayed or used in connection with the Software or the Services constitute trademarks, trade
                            names,
                            company names, or service marks (“Trademarks”) of SydneyDrugs or other entities. Ownership of all
                            Trademarks
                            and the goodwill associated therewith remains with SydneyDrugs or those other entities, and nothing
                            contained in this EULA, the Software, or the Services should be construed as granting any right to use
                            any
                            Trademarks without the prior written consent of the owner of such Trademarks.</p><br/>
                        <p className="text"> The display or use of any third-party Trademarks in connection with the Software or
                            Services does not
                            constitute (a) an affiliation by SydneyDrugs and its licensors with such company; or (b) an endorsement
                            or
                            Approval of such third parties or their products or services.</p><br/>
                        <p className="heading">Uploads and Suggestions</p><br/>
                        <p className="text">The Software and Services may provide functionality through which, or in connection with
                            which, you are able
                            to upload or otherwise transmit or provide information or materials (such as user names, email
                            addresses,
                            passwords, comments, other personal or other data, text, photographs, and images) in connection with
                            your
                            use of the Software or the Services (such as in connection with messages that may be permitted to be
                            transmitted thereby), regardless of whether such information or materials are made available publicly or
                            with password protection (collectively, "Uploads"). You may also elect to provide or make available to
                            SydneyDrugs suggestions, comments, ideas, improvements, or other feedback or similar materials, whether
                            or
                            not related to the Software or the Services (collectively, “Suggestions”). </p><br/>
                        <p className="text"> You hereby grant to SydneyDrugs and its affiliates, representatives, and assigns an
                            irrevocable, perpetual,
                            non-exclusive, fully-paid, world-wide license (sublicensable through multiple tiers) to use, analyze,
                            distribute, reproduce, modify, adapt, derive, publish, translate, publicly perform, publicly display,
                            and
                            otherwise exploit your Uploads and Suggestions (in whole or in part), in any format or medium now known
                            or
                            later developed for any purpose, whether or not in connection with the Software or the Services.</p>
                        <br/>
                        <p className="text">SYDNEYDRUGS WILL NOT EVALUATE YOUR UPLOADS, SUGGESTIONS, OR OTHER COMMUNICATIONS FOR
                            INDICATIONS OF A NEED
                            FOR MEDICAL ATTENTION, AND WILL NOT PROVIDE MEDICAL ADVICE IN RESPONSE. IF YOU CHOOSE TO MAKE ANY OF
                            YOUR
                            PERSONALLY IDENTIFIABLE OR OTHER INFORMATION PUBLICLY AVAILABLE THROUGH THE SOFTWARE OR THE SERVICES,
                            YOU DO
                            SO AT YOUR OWN RISK. </p><br/>
                        <p className="text"> Any Uploads, Suggestions, or other communication or other material (“Other Communications”)
                            that you send
                            through the Software or the Services is and will be deemed to be non-confidential, and SydneyDrugs shall
                            have no obligation of any kind with respect to such information. SydneyDrugs will be free to (but has no
                            obligation to) monitor, analyze, and disclose Uploads, Suggestions, and Other Communications (and to
                            authorize others to do the same) in any manner, and you will gain no right, title, or interest in or to
                            the
                            Software or the Services by virtue of SydneyDrugs doing so. SydneyDrugs shall be free to (but has no
                            obligation to) use any ideas, concepts, know-how, or techniques contained in such Uploads, Suggestions,
                            and
                            Other Communication for any purpose whatsoever, including but not limited to, developing, manufacturing,
                            and
                            marketing products. SydneyDrugs reserves the right to (but has no obligation to) display or insert
                            advertisements in connection with Uploads, Suggestions, and Other Communications, and to use Uploads,
                            Suggestions, and Other Communications for marketing, advertising, and promotional purposes. </p><br/>
                        <p className="text"> You agree that you are solely responsible for all of your Uploads, Suggestions, and Other
                            Communications.
                            SydneyDrugs is not responsible for any loss, theft, or damage of any kind to any Uploads, Suggestions,
                            or
                            Other Communications. You represent and warrant that: (a) you own all rights in your Uploads,
                            Suggestions,
                            and Other Communications or, alternatively, you have acquired all necessary rights in your Uploads,
                            Suggestions, and Other Communications to enable you to grant to SydneyDrugs all of the rights described
                            herein; and (b) your Uploads, Suggestions, and Other Communications do not infringe the intellectual
                            property rights, privacy, or any other legal or moral rights of any third party. You further irrevocably
                            waive any “moral rights” or other rights with respect to attribution of authorship or integrity of
                            materials
                            regarding each Upload, Suggestion, and Other Communication that you may have under any Applicable law
                            under
                            any legal theory.</p><br/>
                        <p className="text">You are prohibited from making Uploads, offering Suggestions, or otherwise using the
                            Software or the
                            Services to transmit any unlawful, threatening, libelous, defamatory, obscene, inflammatory,
                            pornographic,
                            or profane material, or any material that could constitute or encourage conduct that would be considered
                            a
                            criminal offense, give rise to civil liability, or would otherwise violate this EULA, Applicable law, or
                            a
                            copyright, trademark, or other intellectual property or other right of another. SydneyDrugs will
                            cooperate
                            with any law enforcement authorities or court order requesting or directing SydneyDrugs to disclose the
                            identity of anyone transmitting information or materials or otherwise using the Software and the
                            Services.
                            SydneyDrugs may, from time to time, monitor, review, analyze, block, alter, or remove Uploads,
                            discussions,
                            chats, postings, transmissions, bulletin boards, and the like; however SydneyDrugs is under no
                            obligation to
                            do so and assumes no responsibility or liability for the reliability, currency, or completeness of any
                            such
                            Uploads, Suggestions, or Other Communications, nor for any error, defamation, libel, slander, omission,
                            falsehood, obscenity, pornography, profanity, danger, or inaccuracy contained in any Uploads,
                            Suggestions,
                            or Other Communications.</p><br/>
                        <p className="text">SydneyDrugs is not required to host, display, or distribute any Uploads, Suggestions, or
                            Other
                            Communications, and may remove at any time or refuse any Uploads, Suggestions, or Other Communications
                            for
                            any reason. None of the Services are designed or highlighted to be used as a disaster recovery or
                            emergency
                            data storage facility and you are responsible for creating and maintaining copies of your Uploads
                            (including
                            any photos, as Applicable) prior to posting, uploading, or otherwise submitting such Uploads through the
                            Services. </p><br/>
                        <p className="text">SydneyDrugs and its suppliers and vendors are not required to assess or otherwise determine
                            the validity or
                            legitimacy of any complaints or demands that they may receive regarding any Uploads that you may use or
                            allow others to use in connection with the Services (including Uploads posted or submitted to the
                            Services)
                            before SydneyDrugs and its suppliers and vendors take any remedial action that they consider, in their
                            sole
                            discretion, to be Appropriate. </p><br/>
                        <p className="text"> NEITHER SYDNEYDRUGS NOR ITS SUPPLIERS OR VENDORS CONTROL THE UPLOADS, SUGGESTIONS, OR OTHER
                            COMMUNICATIONS
                            POSTED OR SUBMITTED TO THE SERVICES AND DO NOT MONITOR, SCREEN, POLICE, EDIT, OR ACKNOWLEDGE RECEIPT OF
                            THOSE UPLOADS, SUGGESTIONS, OR OTHER COMMUNICATIONS FOR COMPLIANCE WITH APPLICABLE LAWS OR THIS EULA.
                            YOU
                            MAY FIND SOME OF THE UPLOADS, SUGGESTIONS, OR OTHER COMMUNICATIONS POSTED BY OTHER USERS TO BE
                            OFFENSIVE,
                            HARMFUL, INACCURATE, OR DECEPTIVE. YOU SHOULD USE CAUTION AND COMMON SENSE WHEN USING THE SERVICES. </p>
                        <br/>
                        <p className="text">Changes and Updates to the Software or the Services and Compatibility </p><br/>
                        <p className="text">SydneyDrugs will have no obligation to provide the Software or the Services, may change the
                            form and nature
                            of the Software or the Services at any time with or without notice to you, may charge, modify, or waive
                            any
                            fees required to use the Software or the Services (including any portion thereof), and may cease
                            providing
                            the Software or the Services at any time with or without notice to you. SydneyDrugs may choose to
                            provide
                            updates to the Software from time to time in its sole discretion. To the extent that you choose to
                            download
                            or use any such updates to the Software, you acknowledge that you will be bound by the version of this
                            EULA
                            which is Applicable as of the time you download and use any such update to the Software.
                            SydneyDrugs does not represent or warrant that any version of the Software or the Services will be
                            compatible with any hardware, software, or Applications (including any future versions or updates of
                            your
                            phone, tablet, or computer, or any operating system for any such device) or provide the same
                            functionality
                            that is provided by the current version of the Software or the Services. The Software or the Services
                            may
                            not be compatible with your hardware or software versions or Applications (including any specific
                            versions
                            of your phone, tablet, computer, or its specific operating system). SydneyDrugs does not undertake any
                            obligation to provide the Software or the Services to you in a way that is compatible with your hardware
                            or
                            software.</p><br/>
                        <p className="heading">Privacy</p><br/>
                        <p className="text">Your submission of information through the Software is governed by our Privacy Policy
                            (“Privacy Policy”).</p><br/>
                        <p className="text">Anyone with access to your device when you are set to receive reminders may have access to
                            your reminders.
                            If you share a device with others, please do not set the device to receive reminders that you consider
                            to be
                            confidential. Remember to secure access to computers that you use to access the Software or the Services
                            if
                            you want to keep your reminders (or any other information relating to your account) confidential.</p>
                        <br/>
                        <p className="text">IF YOU AUTHORIZE IT AND THE FUNCTIONALITY IS ENABLED AND AVAILABLE, YOUR DESIGNATED
                            CAREGIVER (AS SET UP BY
                            YOU THROUGH THE SOFTWARE OR THE SERVICES) MAY BE ABLE TO VIEW YOUR MEDICAL REMINDERS TO HELP TRACK YOUR
                            PROGRESS. </p><br/>
                        <p className="text"> REMINDERS ABOUT MEDICATIONS AND REPORTS OF RESPONSES TO THESE REMINDERS MAY BE DELAYED OR
                            NOT ACCURATE. YOUR
                            DEVICE, THE SOFTWARE, THE SERVICES, AND THE COMMUNICATIONS BETWEEN THEM MAY NOT ALWAYS FUNCTION AND THE
                            REPORTS DO NOT NECESSARILY REFLECT ACTUAL BEHAVIOR.</p><br/>
                        <p className="text">DO NOT RELY UPON RESPONSES TO MEDICAL REMINDERS WITHOUT VERIFYING ACTUAL BEHAVIORS OF THE
                            INDIVIDUAL
                            INVOLVED. </p><br/>
                        <p className="text">Please note that certain information, including the physical location of your device, may be
                            collected
                            automatically by the Software or the Services, as described in our Privacy Policy. </p><br/>
                        <p className="heading">Monitoring Use </p><br/>
                        <p className="text">SydneyDrugs reserves the right (but does not have the obligation) to elect to electronically
                            monitor use of
                            our Software and our Services (including electronic communications made in connection with the Software
                            and
                            the Services) and may disclose any content, records, or electronic communication of any kind if
                            permitted to
                            do so by any Applicable law, regulation, or government request, if such disclosure is necessary or
                            Appropriate to operate the Software or the Services, or to protect our rights or property, or the rights
                            of
                            the users, partners, affiliates, sponsors, providers, licensors, or merchants. If alerted to allegedly
                            infringing, defamatory, damaging, illegal, or offensive content, we may investigate the allegation and
                            determine in our sole discretion whether to remove or request the removal of such content.</p><br/>
                        <p className="heading">Responsibility for Hardware, Software, Telecommunications, and Other Services </p><br/>
                        <p className="text">You are responsible for obtaining, maintaining, and paying for all hardware, software, and
                            all
                            telecommunications and other services needed for you to use the Software and the Services. </p><br/>
                        <p className="text"> In particular, you acknowledge that by using the internet to use the Software or the
                            Services, you may incur
                            charges from your wireless carrier, internet service provider, or other method of internet access, and
                            that
                            payment of any such charges will be your sole responsibility. SydneyDrugs does not control wireless or
                            internet access. Your use of these networks may not be secure and may expose your personal information
                            sent
                            over such networks. </p><br/>
                        <p className="heading"> Products and Transactions </p><br/>
                        <p className="text">Please note that references to or descriptions or images of products or services through the
                            Software or the
                            Services should not be interpreted as endorsements of such products or services and such products or
                            services may be made available by SydneyDrugs or by third parties. Resale of products or services
                            purchased
                            in connection with the Software or the Services is specifically prohibited. We reserve the right to
                            refuse
                            to sell products or services to you if it reasonably appears to us that you intend to resell such
                            products
                            or services. Verification of information may be required prior to our acceptance of any order. We
                            further
                            reserve the right to limit quantities of items purchased by each customer or to refuse to provide any
                            customer with any such items. Other than for transactions made in a CVS store, your properly completed
                            and
                            delivered order form constitutes your offer to purchase the goods or services referenced in your order,
                            and
                            your order shall be deemed to be accepted only if and when SydneyDrugs or its supplier or vendor sends
                            an
                            order acceptance and shipping notice email to your email address. </p><br/>
                        <p className="text"> Price), quantity, availability of any product or service, and shipping methods and shipping
                            rates, and any
                            other information, descriptions, or images available through the Software or the Services regarding any
                            products or services, are subject to change without notice. Certain weights, measures, and similar
                            descriptions are Approximate and are for convenience only. We seek to undertake reasonable efforts to
                            accurately display the attributes of products and services, including the Applicable colors, however the
                            actual colors that you see will depend on your device, and we cannot guarantee that your device will
                            accurately display such colors. In general, offers available through the Software or the Services are
                            good
                            only while supplies last. It is your responsibility to ascertain and obey all Applicable local, state,
                            federal, and foreign laws (including minimum age requirements) regarding the possession, use, and sale
                            of
                            any products or services available through the Software or the Services. By submitting any information
                            through the Software or the Services in connection with purchasing any products or services, you grant
                            to us
                            the right to provide such information to third parties for purposes of facilitating such purchase.
                            Verification of information may be required prior to the acknowledgment or completion of any
                            transaction.
                            Further terms and conditions related to transactions in connection with the Software or the Services may
                            apply. </p><br/>
                        <p className="text">YOU REPRESENT AND WARRANT THAT YOU HAVE THE RIGHT TO USE ANY PAYMENT CARD OR OTHER METHOD OF
                            PAYMENT THAT
                            YOU SUBMIT THROUGH OR USING THE SOFTWARE OR SERVICES (1) IN CONNECTION WITH A TRANSACTION, AND/OR (2) TO
                            POTENTIALLY BE USED IN CONNECTION WITH ANY FUTURE TRANSACTION(S). </p><br/>
                        <p className="text"> You acknowledge that SydneyDrugs is not responsible for any payment card you may add or use
                            in connection
                            with the Software or the Services and that SydneyDrugs is not a party to any cardholder agreement you
                            may
                            have with your card issuer that governs the use of your payment card. You acknowledge that your card
                            issuer
                            may not enable you to add or use a payment card in connection with the Software or the Services and that
                            you
                            should refer to your cardholder agreement and/or contact your payment card issuer for any questions or
                            disputes you may have relating to your payment card. If you lose the device on which you have downloaded
                            the
                            Software, you should contact your payment card issuer as a precaution to prevent unauthorized access to
                            any
                            payment card that you may have added to a Service. </p><br/>
                        <p className="text"> You agree to pay all charges incurred by you or on your behalf through the Software or the
                            Services, or
                            associated with the device on which you have downloaded the Software, at the prices in effect when such
                            charges are incurred, including all shipping and handling charges. In addition, you are responsible for
                            any
                            taxes Applicable to your transactions. If any payment due is not made by you, SydneyDrugs may, in
                            addition
                            to its other remedies, at its sole discretion and without notice to you, (a) suspend its performance
                            under
                            this EULA and your use of (including any access to) the Software and the Services; or (b) terminate this
                            EULA and your use of (including any access to) the Software and the Services. If legal action is
                            necessary
                            to collect fees or charges due from you, then you will reimburse SydneyDrugs for all expenses incurred
                            in
                            collecting the fees and charges, including all attorney fees and other legal expenses. </p><br/>
                        <p className="heading"> Electronic Communications </p><br/>
                        <p className="text"> When you send electronic communications, including emails or text messages, to us, you are
                            communicating
                            with us electronically and consent to receive return communications, if any, from us electronically,
                            including through the Software or the Services, or otherwise. You agree that all agreements, notices,
                            disclosures, and other communications that we provide to you electronically satisfy any legal
                            requirement
                            that such communications be in writing.</p><br/>
                        <p className="heading"> Payment Features </p><br/>
                        <p className="text">SydneyDrugs may provide you with one or more functionalities that enable you to use your
                            account, the
                            Software, and the Services to enter into certain transactions with us. Certain Services may allow you to
                            add
                            a payment card, such as a credit, debit or prepaid card, which may enable you to make contactless
                            purchase
                            transactions with us. Whether you can make contactless purchase transactions through a Service will
                            depend
                            on, among other things, your location and the issuer of the payment card that you add (or attempt to
                            add) to
                            a Service. The specific functionality made available through a Service is subject to change at any time
                            and
                            SydneyDrugs may cease providing any such functionality at any time. You agree to use any contactless
                            payment
                            functionality that may be available through a Service solely for personal use, and you agree not to use
                            the
                            contactless payment functionality for any illegal or fraudulent use </p><br/>
                        <p className="heading">Resources</p><br/>
                        <p className="text"> Although SydneyDrugs reserves the right to correct any errors, omissions, or inaccuracies,
                            we do not accept
                            any responsibility for the accuracy, reliability, currency, or completeness of any information, Content,
                            materials, services, products, merchandise, functionality, or other resources (collectively,
                            “Resources”)
                            available on or accessible through the Software or the Services (even typographical or imaging errors),
                            including the substance, accuracy, or sufficiency of any service or product information listed on the
                            Software or the Services. Resources may be made available by SydneyDrugs or by third parties, and may be
                            made available for any purpose, including for general information purposes. We make no representations,
                            warranties, or guarantees as to the availability, completeness, accuracy, reliability, validity, or
                            timeliness of any or all of the Resources.</p><br/>
                        <p className="heading">Disclaimer of Warranty </p><br/>
                        <p className="text">THE APP IS PROVIDED TO YOU ON AN “AS IS” AND “AS AVAILABLE” BASIS.</p><br/>
                        <p className="text"> TO THE MAXIMUM EXTENT PERMITTED BY LAW, WE EXPRESSLY DISCLAIM ALL REPRESENTATIONS AND
                            WARRANTIES, WHETHER
                            EXPRESS OR IMPLIED, WITH RESPECT TO THE APP, THE CONTENT AND YOUR ACCESS TO AND OPERATION AND/OR USE
                            THEREOF, INCLUDING BUT NOT LIMITED TO ALL IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A
                            PARTICULAR
                            PURPOSE OR ANY WARRANTIES OF TITLE, NON-INFRINGEMENT AND/OR ARISING FROM A COURSE OF DEALING OR USAGE OF
                            TRADE. </p><br/>
                        <p className="text">WE EXPRESSLY DISCLAIM ALL GUARANTEES, REPRESENTATIONS AND WARRANTIES, WHETHER EXPRESS OR
                            IMPLIED, THAT THE
                            APP WILL MEET YOUR REQUIREMENTS OR THAT YOUR OPERATION AND/OR USE, OR THE RESULTS OF THE OPERATION AND
                            USE
                            OF THE APP WILL BE UNINTERRUPTED, COMPLETE, RELIABLE, ACCURATE, CURRENT, ERROR-FREE, FREE OF VIRUSES OR
                            OTHERWISE SECURE. </p><br/>
                        <p className="text">YOU AGREE THAT YOU DOWNLOAD, INSTALL, OPERATE AND USE THE APP AT YOUR OWN RISK.</p><br/>
                        <p className="heading">DISCLAIMER </p><br/>
                        <p className="text"> SYDNEYDRUGS IS NOT A MEDICAL PRACTITIONER, DOCTOR, QUALIFIED HEALTH CARE PROFESSIONAL OR
                            ANY OTHER QUALIFIED
                            PROFESSIONAL HEALTH CARE PROVIDER. NOTHING CONTAINED IN THIS APP OR THE CONTENT IS INTENDED TO
                            CONSTITUTE OR
                            TO BE USED AS MEDICAL ADVICE AND NOTHING CONTAINED IN THIS APP OR THE CONTENT IS INTENDED TO BE USED TO
                            DIAGNOSE, TREAT, CURE OR PREVENT ANY DISEASE; NOR SHOULD IT BE USED FOR ANY THERAPEUTIC PURPOSES OR AS A
                            SUBSTITUTE FOR THE ADVICE OF YOUR HEALTH CARE PROFESSIONAL. ALL CONTENT PROVIDED ON OR THROUGH THE APP,
                            INCLUDING BUT NOT LIMITED TO THIRD PARTY CONTENT AND ALL OTHER TEXT, GRAPHICS AND IMAGES IS PROVIDED FOR
                            INFORMATIONAL PURPOSES ONLY. NO DOCTOR-PATIENT RELATIONSHIP IS FORMED BETWEEN YOU AND US BY YOUR USE OF
                            THE
                            APP. YOU SHOULD ALWAYS SEEK THE ADVICE OF A DOCTOR, QUALIFIED HEALTH PROFESSIONAL OR OTHER QUALIFIED
                            HEALTH
                            CARE PROVIDER IF YOU HAVE QUESTIONS ABOUT ANY PHYSICAL OR MENTAL HEALTH CONDITION OR ANY OF THE
                            INFORMATION
                            YOU RECEIVE FROM THE APP. YOU SHOULD NEVER DISREGARD OR DELAY SEEKING PROFESSIONAL MEDICAL ADVICE,
                            DIAGNOSIS, OR TREATMENT BASED ON ANYTHING YOU READ OR OBTAIN THROUGH THE APP. SOME OF THE INFORMATION
                            SYDNEYDRUGS PROVIDES MAY BECOME OUTDATED, WHICH MAY RESULT IN IT BEING INCORRECT. HEALTH CARE KNOWLEDGE
                            AND
                            HEALTH CARE PRACTICE CAN EVOLVE AND CHANGE RAPIDLY, AND THEREFORE SYDNEYDRUGS MAKES NO REPRESENTATION OR
                            WARRANTY, EITHER EXPRESS OR IMPLIED, AS TO THE ACCURACY, COMPLETENESS, ADEQUACY, CURRENCY OR TIMELINESS
                            OF
                            THE CONTENT PROVIDED ON THE APP. SYDNEYDRUGS WILL NOT BE LIABLE TO YOU FOR YOUR RELIANCE ON ANY
                            INCORRECT,
                            INACCURATE, INCOMPLETE OR OUTDATED CONTENT ON THE APP. THE APP MAY INCLUDE INTERACTIVE FEATURES THAT
                            ALLOWS
                            USERS OF THE APP TO COMMUNICATE WITH US AND YOU ACKNOWLEDGE THAT: YOU USE THE CONTENT PROVIDED ON THE
                            APP AT
                            YOUR OWN RISK. SYDNEYDRUGS ASSUMES NO LIABILITY OR RESPONSIBILITY FOR ANY ERRORS OR OMISSIONS IN ANY OF
                            THE
                            CONTENT ON THE APP OR THE INFORMATION PROVIDED TO YOU. SYDNEYDRUGS DOES NOT ENDORSE, REPRESENT OR
                            GUARANTEE
                            THE VALIDITY, TRUTHFULNESS, ACCURACY, COMPLETENESS OR RELIABILITY OF ANY OF THE CONTENT ON THE APP. </p>
                        <br/>
                        <p className="heading"> Limitation of Liability </p><br/>
                        <p className="text">THE REMEDIES FOR A BREACH BY US OF THIS EULA OR ANY BREACH BY US OF ANY CONSUMER PROTECTION
                            LAW WILL BE
                            LIMITED TO THE MINIMUM REMEDY THAT MAY BE AVAILABLE IN RESPECT OF ANY SUCH BREACH.</p><br/>
                        <p className="text">YOU AGREE THAT UNDER NO CIRCUMSTANCES WILL SYDNEYDRUGS’S TOTAL LIABILITY FOR ANY LOSS OR
                            DAMAGE SUFFERED OR
                            INCURRED BY YOU EXCEED THE LICENSE FEES (IF ANY) PAID BY YOU FOR THE APP. </p><br/>
                        <p className="text">YOU ACKNOWLEDGE AND AGREE THAT THE LIMITATIONS SET FORTH ABOVE ARE FUNDAMENTAL ELEMENTS OF
                            THIS EULA AND THE
                            SOFTWARE AND THE SERVICES WOULD NOT BE PROVIDED TO YOU ABSENT SUCH LIMITATIONS. </p><br/>
                        <p className="heading"> Indemnification </p><br/>
                        <p className="text">YOU AGREE TO RELEASE AND INDEMNIFY SYDNEYDRUGS, ITS DIRECTORS, OFFICERS, AGENTS, EMPLOYEES,
                            LICENSEES,
                            CONTRACTORS, ASSIGNEES AND SUCCESSORS FROM ALL LOSSES, COSTS, EXPENSES AND DAMAGES OF ANY KIND INCLUDING
                            BUT
                            NOT LIMITED TO DIRECT OR INDIRECT, CONSEQUENTIAL, INCIDENTAL, SPECIAL, EXEMPLARY OR PUNITIVE DAMAGES,
                            AND
                            ANY LIABILITIES ARISING OUT OF OR IN CONNECTION WITH: ANY PAST, PRESENT OR FUTURE CLAIMS, ACTIONS,
                            SUITS,
                            DEMANDS, CAUSES OF ACTION, LIABILITIES AND COSTS OF WHATEVER KIND AND WHEREVER SITUATED WHICH YOU OR ANY
                            THIRD PARTY NOW HAVE OR MAY HAVE ARISING OUT OF OR IN CONNECTION WITH YOUR OPERATION AND USE OF, OR
                            INABILITY TO OPERATE AND USE THE APP ON ANY MOBILE DEVICE THAT YOU OWN OR DO NOT OWN OR CONTROL; A
                            BREACH BY
                            YOU OF YOUR OBLIGATIONS UNDER THIS EULA; ANY WILFUL, UNLAWFUL OR NEGLIGENT ACT OR OMISSION BY YOU; AND
                            ANY
                            USE OR RELIANCE OF THE APP OR CONTENT, OR BY ANY REASON ARISING FROM THIS EULA. </p><br/>
                        <p className="heading"> Termination </p><br/>
                        <p className="text">You may terminate this EULA at any time by destroying all copies of the Software and related
                            documentation
                            under your control and ceasing to use the Services. All rights granted to you under this EULA will
                            immediately terminate if you violate any of the terms of this EULA or if this EULA is terminated, but
                            all
                            other provisions of this EULA (other than with respect to our right to modify this EULA, the Software,
                            or
                            the Services) will survive any such violation or termination. </p><br/>
                        <p className="text">SydneyDrugs or its suppliers or vendors may at any time and for any reason, with or without
                            cause, and in
                            their sole discretion, immediately: (a) suspend or terminate (in whole or in part) your authorization to
                            use
                            the Software or the Services and any membership and account you may have; (b) suspend or terminate and
                            permanently delete and destroy any Access Credential, URL, IP address, or domain name; (c) remove from
                            the
                            Services and permanently delete and destroy any Uploads (or any components thereof) that you or others
                            may
                            have posted or submitted to the Services (and for purposes of this EULA, “posted” and “submitted”
                            includes
                            transmission on or through the Internet and in hardcopy format through facsimile or post) for any reason
                            or
                            no reason; (d) restrict access to the Uploads posted or submitted to the Services and to any account you
                            may
                            have; and (e) prohibit you from any future use of the Software or the Services; all without any prior
                            notice
                            or liability to you or any other person. </p><br/>
                        <p className="text">Without limiting the foregoing, if this EULA is terminated for any reason, then: (a) you may
                            no longer use
                            the Software or the Services, and you must promptly delete the Software from your device and cease all
                            use
                            of any of the Services; (b) this EULA will continue to Apply and be binding upon you in respect of your
                            prior use of the Software and the Services, including payment of any charges accrued in connection with
                            use
                            of the Software and the Services and your indemnification obligations; (c) SydneyDrugs may immediately
                            remove from the Services and permanently delete and destroy any uploads that you or others may have
                            posted
                            or submitted to the Services without any prior notice or liability to you or any other person; and (d)
                            any
                            fees and charges previously paid by you for unused services will not be refunded. </p><br/>
                        <p className="heading"> Governing Law </p><br/>
                        <p className="text">This EULA, your use of the Software and the Services, all transactions through the Software
                            and the
                            Services, and all related matters, regardless of your location, are governed solely by, and construed
                            solely
                            in accordance with, the laws of the State of New South Wales and the Federal laws of Australia and each
                            Party submits to the non-exclusive jurisdiction of the courts of the State of New South Wales, the
                            Federal
                            Courts of Australia (as the case may be) and courts hearing Appeals from those courts.</p><br/>
                        <p className="heading">Contact Us </p><br/>
                        <p className="text"> If you have a question or complaint regarding the Software or the Services, please see here
                            for information
                            on how to contact us, or write to us as follows: </p><br/>
                        <p className="text">SydneyDrugs.com<br/>
                            P.O.Box 725<br/>
                            Westmead Hospital<br/>
                            2145.</p><br/>

                        <p className="heading">Apple-Specific Notice </p><br/>
                        <p className="text">In addition to the other terms set forth herein, and notwithstanding anything to the
                            contrary herein, you
                            acknowledge and agree that the following terms Apply with respect to your use of any version of the
                            Software
                            compatible with the iOS operating system of Apple Inc. (“Apple”): (a) this EULA is between you and
                            SydneyDrugs, and not Apple; (b) SydneyDrugs, and not Apple, is solely responsible for the Software; (c)
                            Apple has no responsibility whatsoever to furnish any maintenance and support services with respect to
                            the
                            Software; (d) in the event of any failure of the Software to conform to any Applicable warranty, you may
                            notify Apple and Apple will refund the purchase price you paid for the Software; (e) to the maximum
                            extent
                            permitted by Applicable law, Apple will have no other warranty obligation whatsoever with respect to the
                            Software; (f) Apple is not responsible for any claims that you have arising out of your use of the
                            Software;
                            (g) Apple will have no responsibility whatsoever for the investigation, defense, settlement, or
                            discharge of
                            any third-party claim that the Software infringes that third party’s intellectual property rights; (h)
                            Apple
                            and its subsidiaries are third-party beneficiaries of this EULA and, upon your acceptance of this EULA,
                            Apple will have the right (and will be deemed to have accepted the right) to enforce this EULA against
                            you
                            as a third party beneficiary; (i) the license that you have been granted herein is limited to a
                            non-transferable license to use the Software on an Apple-branded product that runs Apple’s iOS operating
                            system and is owned or controlled by you, or as otherwise permitted by the Usage Rules set forth in
                            Apple’s
                            App Store Terms of Service; (j) you must comply with the terms of any third-party agreement Applicable
                            to
                            you when using the Software, such as your wireless data service agreement; and (k) any inquiries or
                            complaints relating to the use of the Software, including those pertaining to intellectual property
                            rights,
                            must be directed to SydneyDrugs in accordance with the “Contact Us” section above.</p><br/>
                        <p className="text">Software copyright © [2014]-2017 SydneyDrugs. </p><br/>
                </ModalBody>
                <ModalFooter>
                    <button className="btn btn-primary text-center" onClick={this.acceptTerms}>Accept & Continue</button>
                </ModalFooter>
            </Modal>
        </div>);
    }

}