import React, { Component } from 'react';
import './header.scss';

import logo from '../../../assets/images/Rx.SydneyDrugs-logo.png';

class Header extends Component{

    constructor(props){
        super(props);
        this.toggleNavBar=this.toggleNavBar.bind(this);
    }

    toggleNavBar(e){
        this.props.toggleNavigationBar(e);
    }

    render(){
        return (
            <div>
                <nav className="navbar navbar-default navbar-fixed-top header">
                    <div className="hidden-md-up toggle-button">
                        <a onClick={this.toggleNavBar.bind(this)}>
                            <i className="fa fa-bars toggle-button-bars"/>
                        </a>
                    </div>
                    <img className="rx-logo" alt="" src={logo}/>
                </nav>
            </div>
    );
    }
}

export default Header;

