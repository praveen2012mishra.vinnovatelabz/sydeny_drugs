import React, { Component } from 'react';

//custom css
import './modal-session-expire.scss';

import {
    Modal,
    ModalHeader,
    ModalBody,
} from 'react-modal-bootstrap';

class ModalSessionExpire extends Component {

    constructor(props) {
        super(props);
        this.hideModal = this.hideModal.bind(this);
    }

    hideModal() {
        this.props.closeSessionExpireModal();
    }

    render() {
        return (
            <div className="col-md-12">
                <Modal isOpen={this.props.showModal}
                       onRequestHide={this.hideModal}>
                    <ModalHeader>
                        <h1 className="session-expired">Session expired</h1>
                    </ModalHeader>
                    <ModalBody>

                            <p>           Your session has been inactive for
                                a period of time. For security reasons we have logged you out.
                                You will need to log back in to continue to use the service.</p>

                    </ModalBody>
                </Modal>
            </div>
        );
    }
}

export default ModalSessionExpire;