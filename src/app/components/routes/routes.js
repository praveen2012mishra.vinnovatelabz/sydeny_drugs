/* Vendor components */
import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

/*Custom Components*/
import Main from '../main/main';
import Login from '../login/login';
import Eula from '../eula/eula';

export default class Routes extends Component {

    constructor(props){
        super(props);
        this.handleClick=this.handleClick.bind(this);
    }

    handleClick(route){
        this.props.handleClick(route);
    }

    render() {
        let login=<Login auth={this.props.auth}/>;
        let main = <Main auth={this.props.auth} handleClick={this.handleClick.bind(this)}/>;
        let eula=<Eula auth={this.props.auth}/>;
        return (
            <div>
                <Switch>
                    <Route exact path="/" render={()=>login}/>
                    <Route path="/login" render={()=>login}/>
                    <Route path="/app" render={()=>main}/>
                    <Route path="/eula" render={()=>eula}/>
                </Switch>
            </div>
        );
    }
}