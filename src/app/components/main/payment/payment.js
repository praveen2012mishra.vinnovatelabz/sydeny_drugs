import React, { Component } from 'react';
import { getCards,deleteCard,makePayment } from './payment-service';

import Card from './card/card-payment';
import history from '../../history';

import './payment.scss';

export default class Payment extends Component {

    constructor(props) {
        super(props);
        this.state = {cards: [],selectedCard:{},mode:'default',invoiceId:''};
        this.deleteSelectedCard = this.deleteSelectedCard.bind(this);
        this.selectedCard = this.selectedCard.bind(this);
        this.addCard = this.addCard.bind(this);
        this.payment=this.payment.bind(this);
    }

    componentDidMount() {
        getCards().then((response) => {
            if (this.refs.payment)
                this.setState({cards: response});
        });

        if (history.location.state) {
            let invoiceId= history.location.state.id;
            let mode = history.location.state.mode;
            this.setState({mode: mode,invoiceId:invoiceId});
        }
    }

    deleteSelectedCard(selectedCard){
        let card = {
            cardId: selectedCard.id
        };
        deleteCard(card).then((response)=>{
            if(response){
                let cards = this.state.cards;
                let remainingCards=cards.filter((card)=>{
                    return card.id !== selectedCard.id;
                });
                if(this.refs.payment)
                    this.setState({cards:remainingCards});
            }
        });
    }

    payment(selectedCard) {
        let card = {
            cardId: selectedCard.id,
            invoiceId: this.state.invoiceId
        };
        makePayment(card).then((response) => {
            alert("Payment Successfull");
            if (response) {
                history.push('/app/invoice');
            }
        });
    }

    selectedCard(selectedCard) {
        if(this.refs.payment)
            this.setState({selectedCard: selectedCard});
    }

    addCard(card) {
        let cards = this.state.cards;
        cards.push(card);
        if(this.refs.payment)
            this.setState({cards:cards});
    }

    render() {
        let cards = this.state.cards;
        let cardsView = [];
        let cardsViewMobile = [];
        for (let i in cards) {
            let card = cards[i];
            let brand = <span/>;
            if (card.brand === 'Visa') {
                brand = <span className="fa fa-cc-visa card-type"/>;
            } else if (card.brand === 'MasterCard') {
                brand = <span className="fa fa-cc-mastercard card-type"/>;
            } else if (card.brand === 'Diners') {
                brand = <span className="fa fa-cc-diners-club card-type"/>;
            } else if (card.brand === 'Discover') {
                brand = <span className="fa fa-cc-discover card-type"/>;
            } else if (card.brand === 'Amex') {
                brand = <span className="fa fa-cc-amex card-type"/>;
            }
            let deleteClass=this.state.selectedCard.id === card.id?'btn btn-danger active':'btn btn-danger disabled';

            let paymentClass=this.state.selectedCard.id === card.id?'btn btn-success active':'btn btn-success disabled';

            let wrapperClassName=this.state.mode ==='payment'?
                'col-md-12 col-sm-12 existing-card-payment'
                : 'col-md-12 col-sm-12 existing-card-data';

            let cardView = (
                <div key={i} className={ wrapperClassName }>
                    <div className="col-md-4 col-sm-4 card-name">
                        <input type="radio" onChange={this.selectedCard.bind(this,card)} checked={this.state.selectedCard.id === card.id}/>
                        {brand}
                        <span><span>{card.brand} ending in {card.last4}</span></span>
                    </div>
                    <div className="col-md-3 col-sm-3">
                        {card.name}
                    </div>
                    <div className="col-md-3 col-sm-3">
                        {card.exp_month}/{card.exp_year}
                    </div>

                    {
                        this.state.mode==='payment' ?
                            <div className="col-md-2 col-sm-2">
                                <div className={paymentClass} onClick={this.payment.bind(this,card)} style={{marginLeft: "2px"}} >Pay</div>
                            </div>:
                            <div className="col-md-2 col-sm-2">
                                <div className={deleteClass} style={{marginLeft: "2px"}} onClick={this.deleteSelectedCard.bind(this,card)}>Delete</div>
                            </div>
                    }
                </div>);
            cardsView.push(cardView);
        }

        for (let j in cards) {
            let card = cards[j];
            let brand = <span/>;
            if (card.brand === 'Visa') {
                brand = <span className="fa fa-cc-visa card-type"/>;
            } else if (card.brand === 'MasterCard') {
                brand = <span className="fa fa-cc-mastercard card-type"/>;
            } else if (card.brand === 'Diners') {
                brand = <span className="fa fa-cc-diners-club card-type"/>;
            } else if (card.brand === 'Discover') {
                brand = <span className="fa fa-cc-discover card-type"/>;
            } else if (card.brand === 'Amex') {
                brand = <span className="fa fa-cc-amex card-type"/>;
            }
            let deleteClass=this.state.selectedCard.id === card.id?'btn btn-danger active':'btn btn-danger disabled';

            let paymentClass=this.state.selectedCard.id === card.id?'btn btn-success active':'btn btn-warning disabled';

            let wrapperClassName=this.state.mode ==='payment'?
                'col-xs-12 existing-card-payment'
                : 'col-xs-12 existing-card-data';

            let cardView = (
                <div key={j} className={wrapperClassName}>
                    <div className="col-xs-12 p-0">
                        <div className="col-xs-4 card-header p-b-6">Your saved card</div>
                        <div className="col-xs-8 card-name divider p-b-6">
                            <input type="radio" onChange={this.selectedCard.bind(this,card)} checked={this.state.selectedCard.id === card.id}/>
                            {brand}
                            <span><span>{card.brand} ending in {card.last4}</span></span>
                        </div>
                    </div>
                    <div className="col-xs-12 p-0">
                        <div className="col-xs-4 card-header p-b-6">Name on card</div>
                        <div className="col-xs-8 divider p-b-6">{card.name}</div>
                    </div>
                    <div className="col-xs-12 p-0">
                        <div className="col-xs-4 card-header p-b-6">Expires on</div>
                        <div className="col-xs-8 divider p-b-6">{card.exp_month}/{card.exp_year}</div>
                    </div>

                    {
                        this.state.mode==='payment' ?
                            <div className="col-md-2 col-sm-2">
                                <div className={paymentClass} onClick={this.payment.bind(this,card)} style={{marginLeft: "2px"}} >Pay</div>
                            </div>:
                            <div className="col-md-2 col-sm-2">
                                <div className={deleteClass} style={{marginLeft: "2px"}} onClick={this.deleteSelectedCard.bind(this,card)}>Delete</div>
                            </div>
                    }
                </div>);
            cardsViewMobile.push(cardView);
        }

        return (
            <div className="container payment" ref="payment">
                <div className="row">
                    <div className="col-md-12 view-header">
                        <h3>You can add and delete your credit cards here</h3>
                    </div>
                    <div className="card-view">
                        <div className="col-md-12 card-layout">
                            <div className="existing-cards">
                                <div className="row hidden-xs">
                                    <div className="col-md-12 col-sm-12 existing-card-header">
                                        <div className="col-md-4 col-sm-4">
                                            Your saved credit cards
                                        </div>
                                        <div className="col-md-3 col-sm-3">
                                            Name on card
                                        </div>
                                        <div className="col-md-3 col-sm-3">
                                            Expires on
                                        </div>
                                        <div className="col-md-2 col-sm-2"/>
                                    </div>
                                </div>
                                <div className="row hidden-xs-down">
                                    {cardsView}
                                </div>
                                <div className="row hidden-sm-up">
                                    {cardsViewMobile}
                                </div>
                            </div>
                            <div className="row new-card">
                                <Card addCard={this.addCard.bind(this)} payment={true}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}