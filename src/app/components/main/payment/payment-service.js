export function getCards() {
    return (fetch('api/user/card/get', {method: "GET"}).then((response) => response.json()));
}

export function deleteCard(card) {
    return (
        fetch('api/user/card/delete', {
            method: "POST",
            headers: {"content-type": "application/json"},
            body: JSON.stringify(card)
        }).then((response) => {
            return response.json();
        })
    );
}

export function makePayment(cardId,invoiceID) {
    return (
        fetch('api/user/makepayment', {
            method: "POST",
            headers: {"content-type": "application/json"},
            body: JSON.stringify(cardId,invoiceID)
        }).then((response) => {
            return response.json();
        })
    );
}
