
export function addCard(req){
    return(fetch('/api/user/card/add',{method:"POST",headers:{"content-type":"application/json"},body:JSON.stringify(req)}).then((response)=>{
        return response.json();
    }));
}
