import React,{Component} from 'react';
import * as ConfigService from '../../../../config/config-service';

import './card.scss';
let card;
let stripe;
export default class CardMemberShip extends Component{

    constructor(props){
        super(props);
        this.state={errorMessage:''};
        this.createCardTemplate = this.createCardTemplate.bind(this);
        this.createCard = this.createCard.bind(this);
        this.addCardToStripe = this.addCardToStripe.bind(this);
    }

    componentDidMount(){
        let response = ConfigService.getStripeConfig();
        if (this.refs.cardMembership) {
            this.createCardTemplate(response.subscriptionStripePublishableKey);
        }
    }

    createCardTemplate(publishablekey){
        stripe =Stripe(publishablekey);
        let elements = stripe.elements();
        card = elements.create('card', {
            style: {
                base: {
                    iconColor: '#666EE8',
                    color: '#31325F',
                    lineHeight: '40px',
                    fontWeight: 300,
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSize: '15px',

                    '::placeholder': {
                        color: '#CFD7E0',
                    },
                },
            },
            hidePostalCode: true
        });
        card.mount('#card-element-membership');
    }

    createCard(){
        stripe.createToken(card, {name: this.refs.cardName.value}).then(this.addCardToStripe.bind(this));
    }

    addCardToStripe(result){
        if (result.token) {
            let req = {token: result.token.id};
            this.props.addCard(req);
        } else if (result.error) {
            this.setState({errorMessage:result.error.message});
        }
    }

    render(){
        return(<div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 addCardForm" ref="cardMembership">
            <div className="addCardForm-container">
                <div className="col-md-12">
                    <h3> Another payment method</h3>
                </div>

                <div className="col-md-12" style={{marginTop:"15px"}}>
                    <form>
                        <div className="group">
                            <label>
                                <span>Name</span>
                                <input ref="cardName" className="field"/>
                            </label>
                            <label>
                                <span>Card</span>
                                <div id="card-element-membership" className="field"/>
                            </label>
                        </div>
                        {this.state.errorMessage?<p className="alert-danger error-message">{this.state.errorMessage}</p>:<p/>}
                        <button onClick={this.createCard.bind(this)}>Add Card</button>
                    </form>
                </div>
            </div>

        </div>);
    }
}