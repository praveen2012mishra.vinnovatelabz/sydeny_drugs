import React,{Component} from 'react';
import { addCard } from './card-service';
import * as ConfigService from '../../../../config/config-service';

import './card.scss';
let card;
let stripe;
export default class Card extends Component{

    constructor(props){
        super(props);
        this.state={errorMessage:''};
        this.createCardTemplate = this.createCardTemplate.bind(this);
        this.createCard = this.createCard.bind(this);
        this.addCardToStripe = this.addCardToStripe.bind(this);
    }

    componentDidMount(){
        let response = ConfigService.getStripeConfig();
        if (this.refs.cardPayment) {
            this.createCardTemplate(response.paymentStripePublishableKey);
        }
    }

    createCardTemplate(publishablekey){
        stripe =Stripe(publishablekey);
        let elements = stripe.elements();
        card = elements.create('card', {
            style: {
                base: {
                    iconColor: '#666EE8',
                    color: '#31325F',
                    lineHeight: '40px',
                    fontWeight: 300,
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSize: '15px',

                    '::placeholder': {
                        color: '#CFD7E0',
                    },
                },
            },
            hidePostalCode: true
        });
        card.mount('#card-element');
    }

    createCard(){
         stripe.createToken(card, {name: this.refs.name.value}).then(this.addCardToStripe);
    }

    addCardToStripe(result){
        if (result.token) {
            let req = {token: result.token.id};
            addCard(req).then((response) => {
                this.props.addCard(response);
                window.alert('Added Card');
                card.clear();
                this.refs.name.value = '';
            });
        } else if (result.error) {
            if(this.refs.cardPayment)
            this.setState({errorMessage:result.error.message});
        }
    }

    render(){
        return(<div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 addCardForm" ref="cardPayment">
            <div className="addCardForm-container">
                <div className="col-md-12">
                    <h3> Another payment method</h3>
                </div>

                <div className="col-md-12" style={{marginTop:"15px"}}>
                    <form>
                        <div className="group">
                            <label>
                                <span>Name</span>
                                <input ref="name" className="field"/>
                            </label>
                            <label>
                                <span>Card</span>
                                <div id="card-element" className="field"/>
                            </label>
                        </div>
                        {this.state.errorMessage?<p className="alert-danger error-message">{this.state.errorMessage}</p>:<p/>}
                        <button onClick={this.createCard}>Add Card</button>
                    </form>
                </div>
            </div>

        </div>);
    }
}