import React, { Component } from 'react';
import history from '../../history';

/*Custom Components*/
import Link from '../link/link';

/*Images*/
import paymentIcon from '../../../../assets/images/icons/payment-icon.png';
import membershipIcon from '../../../../assets/images/icons/member-icon.png';
import invoiceIcon from '../../../../assets/images/icons/invoices.png';
import shippingIcon from '../../../../assets/images/icons/shipping.png';
import supportIcon from '../../../../assets/images/icons/support-icon.png';
import logout from '../../../../assets/images/icons/Logout.png';
import intercomIcon from '../../../../assets/images/icons/Intercom-icon.svg';

// custom scss
import './leftNavBar.scss';

export default class LeftNavBar extends Component {
    constructor(props){
        super(props);
        this.state = {activeLink: '/app/dashboard',chatActive:false};
        this.setActive = this.setActive.bind(this);
        this.isActive = this.isActive.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.logout = this.logout.bind(this);
        this.showIntercom=this.showIntercom.bind(this);
    }

    componentDidMount(){
        this.setState({activeLink:history.location.pathname});
    }

    setActive(selectedLink) {
        this.setState({activeLink: selectedLink});
    }

    componentWillReceiveProps(){
        this.setState({activeLink: history.location.pathname});
    }

    isActive(navOption) {
        if (navOption === this.state.activeLink) {
            return "active-link";
        }
    }

    handleClick(route){
        this.setState({activeLink: route.to,chatActive:false});
        this.props.handleClick(route);
    }

    logout(){
        localStorage.clear();
        Intercom('shutdown');
        history.push('/login');
    }

    showIntercom(){
        Intercom('show');
        this.setState({chatActive:true});
    }

    render() {
        let userPicture="";
        let userName='';
        if(localStorage.profile){
            let obj=JSON.parse(localStorage.profile);
            userName=obj.nickname;
            userPicture=obj.picture;
        }
        let home=<i className="fa fa-home home-icon"/>;
        return (<div>
            <ul className="navigation-links text-center">
                <li className="user"><div>
                    <img className="user-picture" src={userPicture}/>
                    <span className="link-name user-name">{userName}</span>
                </div>
                </li>

                <li className={this.isActive('/app/dashboard')}>
                    <Link to="/app/dashboard" authorised={true} handleClick={this.handleClick.bind(this)} icon={home} name="Home"/>
                </li>
                <li className={this.isActive('/app/payment')}>
                    <Link to="/app/payment" authorised={false} handleClick={this.handleClick.bind(this)} image={paymentIcon} name="Payment"/>
                </li>
                <li className={this.isActive('/app/membership')}>
                    <Link to="/app/membership" authorised={true} handleClick={this.handleClick.bind(this)} image={membershipIcon} name="Membership"/>
                </li>
                <li className={this.isActive('/app/invoice')}>
                    <Link to="/app/invoice" authorised={true} handleClick={this.handleClick.bind(this)} image={invoiceIcon} name="invoice"/>
                </li>
                <li className={this.isActive('/app/shipping')}>
                    <Link to="/app/shipping" authorised={true} handleClick={this.handleClick.bind(this)} image={shippingIcon} name="Shipping"/>
                </li>
                <li className={this.isActive('/app/support')}>
                    <a href="https://support.sydneydrugs.com" target="_blank"
                       onClick={this.setActive.bind(this, '/app/support')}>
                        <div className="navigation-link">
                            <img className="left-nav-bar-icon" src={supportIcon}/>
                            <span className="link-name">Support</span>
                        </div>
                    </a>
                </li>
                <li className={this.state.chatActive?"active-link":''} onClick={this.showIntercom}>
                    <button className="chat-button">
                        <img className="chat-img" src={intercomIcon}/>
                        <span className="chat-name">Chat</span>
                    </button>
                </li>
                <li onClick={this.logout}>
                    <button className="logout-button">
                        <img className="logout-img" src={logout}/>
                        <span className="logout-name">Logout</span>
                    </button>
                </li>
            </ul>
        </div>);
    }
}