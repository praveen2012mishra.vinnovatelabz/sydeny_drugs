import React,{ Component } from 'react';

/*DataTable js*/
import '../../../../../assets/data-table/css/responsive';

/*custom scss*/
import '../invoice.scss';

/*Datatable scss*/
import '../../../../../assets/data-table/css/responsive.scss';
import '../../../../../assets/data-table/css/data-table.scss';

const $=require('jquery');
$.DataTable=require('datatables.net');

export default class PaidInvoice extends Component {
    constructor(props){
        super(props);
        this.tableData=this.tableData.bind(this);
        this.insertToDataTable= this.insertToDataTable.bind(this);
        this.convertTimeFormat=this.convertTimeFormat.bind(this);
        this.changeAmount=this.changeAmount.bind(this);
        this.paidRender=this.paidRender.bind(this);
    }

    componentDidMount(){
        this.$el=$(this.el);
        let paidInvoices= this.props.paidInvoices;
        let data=this.tableData(paidInvoices);
        this.insertToDataTable(data);
    }

    componentWillReceiveProps(nextProps) {
        let paid = JSON.stringify(this.props.paidInvoices) === JSON.stringify(nextProps.paidInvoices);
        if (!paid) {
            this.$el = $(this.el);
            let paidInvoices = nextProps.paidInvoices;
            let data = this.tableData(paidInvoices);
            this.insertToDataTable(data);
        }
    }

    insertToDataTable(data){
        this.$el.DataTable(
            {
                data: data,
                columns: [
                    { title: "Date" },
                    { title: "Amount Due" },
                    { title: "Currency" },
                    { title: "Description" },
                    { title: "Paid" }
                ],
                responsive:true,
            }
        );
    }

    changeAmount(amount){
        return (amount / 100);
    }

    convertTimeFormat(time){
        let data=time*1000;
        let timezone=new Date(data);
        timezone=timezone.toLocaleDateString();
        return timezone;
    }

    paidRender(data) {
        return data ? 'Yes' : 'No';
    }

    tableData(invoices) {
        let dataContainer = [];
        let rowData = [];
        let self=this;


        invoices.data.map(function (data) {
            let date = self.convertTimeFormat(data.created);
            rowData.push(date);

            let amount = self.changeAmount(data.amount);
            rowData.push(amount);

            let currency = data.currency.toUpperCase();
            rowData.push(currency);

            rowData.push(data.description);

            let paid = self.paidRender(data.paid);
            rowData.push(paid);

            dataContainer.push(rowData);
        });
        return dataContainer;
    }

    render() {
        return (<div>
            <table className="display" width="100%" ref={el => this.el = el}/>
        </div>);
    }
}