import React,{ Component } from 'react';

/*Custom Services*/
import { getInvoices,getPendingInvoices } from './invoice-service';

/*Custom componets*/
import PaidInvoice from './paid-invoices/paidInvoice';
import PendingInvoice from './pending-invoices/pendingInvoice';

/*custom scss*/
import './invoice.scss';

export default class Invoice extends Component {
    constructor(props){
        super(props);
        this.state={paidInvoices:'',pendingInvoices:''};
    }

    componentDidMount(){
        getInvoices().then((response)=>{
            this.setState({paidInvoices:response});
        });

        getPendingInvoices().then((response)=>{
            this.setState({pendingInvoices:response});
        });
    }

    render() {
        return (
            <div className="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                {
                    this.state.paidInvoices ? <div>
                        <h3 className="paid-invoice-header">Paid Invoice</h3>
                        <div className="paid-invoice">
                            <PaidInvoice paidInvoices={this.state.paidInvoices}/>
                        </div>
                    </div> : null
                }
                {
                    this.state.pendingInvoices ? <div>
                        <h3 className="pending-invoice-header">Pending Invoice</h3>
                        <div className="unpaid-invoice">
                            <PendingInvoice pendingInvoices={this.state.pendingInvoices}/>
                        </div>
                    </div> : null
                }
            </div>);
    }
}