
export function getInvoices() {
    return (fetch('/api/user/charges',{method:"GET"}).then((response)=> response.json()));
}

export function getPendingInvoices(){
    return (fetch('/api/user/pendinginvoices',{method:"GET"}).then((response)=> response.json()));
}