import React,{ Component } from 'react';
import history from "../../../history";

/*Custom scss*/
import '../invoice.scss';

/*datatables scss*/
import '../../../../../assets/data-table/css/responsive.scss';
import '../../../../../assets/data-table/css/data-table.scss';

/*Datatable js*/
import '../../../../../assets/data-table/css/responsive';

const $=require('jquery');
$.DataTable=require('datatables.net');

export default class PendingInvoice extends Component {
    constructor(props){
        super(props);
        this.tableData=this.tableData.bind(this);
        this.insertToDataTable= this.insertToDataTable.bind(this);
        this.convertTimeFormat=this.convertTimeFormat.bind(this);
        this.changeAmount=this.changeAmount.bind(this);
        this.paidRender=this.paidRender.bind(this);
        this.handleClick=this.handleClick.bind(this);
    }

    componentDidMount(){
        this.$el=$(this.el);
        let unPaidInvoices= this.props.pendingInvoices;
        let data=this.tableData(unPaidInvoices);
        this.insertToDataTable(data);
    }

    componentWillReceiveProps(nextProps){
        let unpaid=JSON.stringify(this.props.pendingInvoices) === JSON.stringify(nextProps.pendingInvoices);
        if(!unpaid){
            this.$el=$(this.el);
            let unPaidInvoices= nextProps.pendingInvoices;
            let data=this.tableData(unPaidInvoices);
            this.insertToDataTable(data);
        }
    }

    insertToDataTable(data){
        this.$el.DataTable(
            {
                data: data,
                columns: [
                    { title: "Date" },
                    { title: "Amount Due" },
                    { title: "Currency" },
                    { title: "Billing" },
                    { title: "Paid" },
                    { title: "Action" }
                ],
                responsive:true,
            }
        );
    }

    changeAmount(amount){
        return (amount / 100);
    }

    convertTimeFormat(time){
        let data=time*1000;
        let timezone=new Date(data);
        timezone=timezone.toLocaleDateString();
        return timezone;
    }

    paidRender(data) {
        return data ? 'Yes' : 'No';
    }

    handleClick(e){
        if(e.target.type){
            if(e.target.type==='button')
                history.push({
                    pathname: '/app/payment',
                    state: { id: e.target.value,mode:'payment'}
                });
        }
    }

    tableData(invoices) {
        let dataContainer = [];
        let self = this;
        invoices.map(function (data) {
            let rowData = [];
            let date = self.convertTimeFormat(data.date);
            rowData.push(date);

            let amount = self.changeAmount(data.amount_due);
            rowData.push(amount);

            let currency = data.currency.toUpperCase();
            rowData.push(currency);

            rowData.push(data.billing);

            let paid = self.paidRender(data.paid);
            rowData.push(paid);

            let id = data.id;
            let btn = "<button class='action-btn btn-success' type='button' value=" + id + ">Pay</button>";
            rowData.push(btn);

            dataContainer.push(rowData);
        });
        return dataContainer;
    }

    render() {
        return (<div>
            <table onClick={this.handleClick.bind(this)} className="display" width="100%" ref={el => this.el = el}/>
        </div>);
    }
}