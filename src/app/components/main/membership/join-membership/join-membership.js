import React, {Component} from 'react';
import Card from '../../payment/card/card-membership';
import {createSubscription} from './join-membership-service';
import history from '../../../history';
import './join-membership.scss';

export default class JoinMembership extends Component {

    constructor(props){
        super(props);
        this.state={plan:{name:'',id:''}};
        this.addPayment = this.addPayment.bind(this);
    }

    componentDidMount(){
        if(history.location.state){
            this.setState({plan:history.location.state});
        }else{
            history.push('/app/membership');
        }
    }

    addPayment(card){
        card.planId=this.state.plan.id;
        createSubscription(card).then(()=>{
           history.push('/app/membership');
        });
    }

    render() {
        return (<div className="join-membership">
            <div className="container joinmembership">

                <div className="row">
                    <div className="col-md-12">
                        <h2 className="selected-plan-header"><span>Selected Plan</span> : <span>{this.state.plan.name}</span></h2>
                    </div>

                    <Card addCard={this.addPayment.bind(this)}/>

            </div>

        </div>
        </div>);
    }
}