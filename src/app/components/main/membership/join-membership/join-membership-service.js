
export function createSubscription(req) {

    return (fetch("/api/user/subscription/create", {
        method: "POST",
        headers: {"content-type": "application/json"},
        body: JSON.stringify(req)
    }).then((response) => response.json()));
}