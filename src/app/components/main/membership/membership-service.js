export function getSubscription() {
    return (fetch('/api/user/subscription/get', {method: "GET"}).then((response) => {
        return response.json();
    }));
}

export function changeSubscriptions(req){
    return(fetch("/api/user/subscription/change",{method:"POST",headers:{"content-type":"application/json"},body:JSON.stringify(req)}).then((response)=>response.json()));
}

export function subscriptionInvoices() {
    return (fetch('/api/user/subscription/invoices', {method: "GET"}).then((response) => {
        return response.json();
    }));
}
