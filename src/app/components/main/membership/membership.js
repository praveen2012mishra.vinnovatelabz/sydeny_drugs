import React, { Component } from 'react';
import { getSubscription,changeSubscriptions,subscriptionInvoices } from './membership-service';
import MembershipInvoice from './membership-invoices/membership-invoice';
import classNames from 'classnames';
import history from '../../history';

// custom scss
import './membership.scss';

const SUBSCRIPTION_PLANS = {
    'BASIC': {'name': 'Basic', 'id': '1'},
    'GOLD': {'name': 'Gold', 'id': '2'},
    'VIP': {'name': 'VIP', 'id': '3'},
    'ENTERPRISE': {'name': 'Enterprise', 'id': '4'},
};

export default class Membership extends Component {

    constructor(props){
        super(props);
        this.state={subscriptions:null,subscriptionInvoices:null};
        this.subscribe = this.subscribe.bind(this);
        this.downGrade = this.downGrade.bind(this);
        this.upGrade = this.upGrade.bind(this);
        this.convertTimeStamp = this.convertTimeStamp.bind(this);
    }

    componentDidMount(){

        subscriptionInvoices().then((response)=>{
            this.setState({subscriptionInvoices:response});
        });

        getSubscription().then((response)=>{
            this.setState({subscriptions:response.data});
        });
    }

    subscribe(plan) {
        history.push({pathname:"/app/membership/join",state:plan});
    }

    downGrade(planId) {
        let req = {};
        req.planId = planId;
        changeSubscriptions(req).then((response)=>{
            window.alert('plan downgraded');
            this.setState({subscriptions:[response]});
        });
    }

    upGrade(planId) {
        let req = {};
        req.planId = planId;
        changeSubscriptions(req).then((response)=>{
            window.alert('plan updated');
            this.setState({subscriptions:[response]});
        });
    }

    convertTimeStamp(time){
        let data=time*1000;
        let timezone=new Date(data);
        timezone=timezone.toLocaleDateString();
        return timezone;
    }

    render() {
        let subscriptions= this.state.subscriptions;
        if(subscriptions){
            let isSubscribed = subscriptions.length>0;
            let subscribedPlanId = (isSubscribed?(subscriptions[0].plan.id):0);

            return (<div className="container-fluid membership-plan" ref="membership">
                <div className="row">
                    <div className="col-md-12 offset-lg-1 col-lg-10 col-sm-12 col-xs-12">
                        <div className="row">
                            <div className="col-md-12 plan-header">
                                Choose your SydneyDrugs Plan
                            </div>
                        </div>

                        <div className="plan-block">
                            <div className={classNames('col-md-4 col-sm-12 col-xs-12 plan-card',{'plan-card-selected':subscribedPlanId===SUBSCRIPTION_PLANS.BASIC.id})}>

                                <div className="plan-name">
                                    Basic
                                </div>
                                <div className="plan-price">
                                    $99
                                </div>
                                <div className="plan-desc">
                                    Access to prescription medication for the whole family and pets.
                                </div>
                                <div className="plan-desc">
                                    Prescription reminders
                                </div>
                                <div className="plan-desc">
                                    Unbiased pharmacist advice
                                </div>
                                <div className="plan-desc">
                                    Access to pet medications
                                </div>
                                <div className="plan-desc">
                                    Wound care dressings
                                </div>

                                {
                                    !isSubscribed ? <div className="subscribe-blk">
                                        <div className="btn btn-primary subscribe-btn" onClick={this.subscribe.bind(this,SUBSCRIPTION_PLANS.BASIC)}>
                                            Subscribe
                                        </div>
                                    </div> : <div/>
                                }

                                {
                                    (isSubscribed && subscribedPlanId===SUBSCRIPTION_PLANS.BASIC.id)?
                                        <div className="plan-selected">
                                            <div className="current-plan">Your current plan</div>
                                            <div className="plan-expiry">Expires on {this.convertTimeStamp(subscriptions[0].current_period_end)}
                                            </div>
                                        </div>:<div/>
                                }
                                {
                                    (isSubscribed && subscribedPlanId!==SUBSCRIPTION_PLANS.BASIC.id)?
                                        <div className="downgrade-block">
                                            <div className="btn btn-primary downgrade-btn" onClick={this.downGrade.bind(this,SUBSCRIPTION_PLANS.BASIC.id)}>
                                                Downgrade to Basic
                                            </div>
                                        </div>:<div/>
                                }

                            </div>

                            <div className={classNames('col-md-4 col-sm-12 col-xs-12 plan-card',{'plan-card-selected':subscribedPlanId===SUBSCRIPTION_PLANS.GOLD.id})}>
                                <div className="plan-name">
                                    Gold
                                </div>
                                <div className="plan-price">
                                    $199
                                </div>
                                <div className="plan-desc">
                                    Everything in the Basic plan
                                </div>
                                <div className="plan-desc">
                                    Compounded prescriptions
                                </div>
                                <div className="plan-desc">
                                    SMS conversation with pharmacist
                                </div>
                                <div className="plan-desc">
                                    Priority support
                                </div>
                                <div className="plan-desc">
                                    Compounded pet prescriptions
                                </div>
                                <div className="plan-desc">
                                    Special orders
                                </div>
                                {
                                    !isSubscribed?<div className="subscribe-blk">
                                        <div className="btn btn-primary subscribe-btn" onClick={this.subscribe.bind(this,SUBSCRIPTION_PLANS.GOLD)}>
                                            Subscribe
                                        </div>
                                    </div>:<div/>
                                }

                                {
                                    (isSubscribed && subscribedPlanId===SUBSCRIPTION_PLANS.GOLD.id)?
                                        <div className="plan-selected">
                                            <div className="current-plan">Your current plan</div>
                                            <div className="plan-expiry">Expires on {this.convertTimeStamp(subscriptions[0].current_period_end)}
                                            </div>
                                        </div>:<div/>
                                }

                                {
                                    (isSubscribed && (subscribedPlanId===SUBSCRIPTION_PLANS.BASIC.id))?
                                        <div className="upgrade-block">
                                            <div className="btn btn-primary upgrade-btn" onClick={this.upGrade.bind(this,SUBSCRIPTION_PLANS.GOLD.id)}>
                                                Upgrade to Gold
                                            </div>
                                        </div>:<div/>
                                }

                                {
                                    (isSubscribed && (subscribedPlanId===SUBSCRIPTION_PLANS.VIP.id))?
                                        <div className="downgrade-block">
                                            <div className="btn btn-primary downgrade-btn" onClick={this.downGrade.bind(this,SUBSCRIPTION_PLANS.GOLD.id)}>
                                                Downgrade to Gold
                                            </div>
                                        </div>:<div/>
                                }

                            </div>

                            <div className={classNames('col-md-4 col-sm-12 col-xs-12 plan-card',{'plan-card-selected':subscribedPlanId===SUBSCRIPTION_PLANS.VIP.id})}>

                                <div className="plan-name">
                                    VIP
                                </div>
                                <div className="plan-price">
                                    $499
                                </div>
                                <div className="plan-desc">
                                    Everything in the Gold plan
                                </div>
                                <div className="plan-desc">
                                    1 free delivery every month
                                </div>
                                <div className="plan-desc">
                                    Priority service
                                </div>
                                <div className="plan-desc">
                                    Inbound phone calls
                                </div>
                                <div className="plan-desc">
                                    Medication reviews
                                </div>

                                {
                                    !isSubscribed?<div className="subscribe-blk">
                                        <div className="btn btn-primary subscribe-btn" onClick={this.subscribe.bind(this,SUBSCRIPTION_PLANS.VIP)}>
                                            Subscribe
                                        </div>
                                    </div>:<div/>
                                }

                                {
                                    (isSubscribed && subscribedPlanId===SUBSCRIPTION_PLANS.VIP.id)?
                                        <div className="plan-selected">
                                            <div className="current-plan">Your current plan</div>
                                            <div className="plan-expiry">Expires on {this.convertTimeStamp(subscriptions[0].current_period_end)}
                                            </div>
                                        </div>:<div/>
                                }

                                {
                                    (isSubscribed && (subscribedPlanId===SUBSCRIPTION_PLANS.BASIC.id || subscribedPlanId===SUBSCRIPTION_PLANS.GOLD.id))?
                                        <div className="upgrade-block">
                                            <div className="btn btn-primary upgrade-btn" onClick={this.upGrade.bind(this,SUBSCRIPTION_PLANS.VIP.id)}>
                                                Upgrade to VIP
                                            </div>
                                        </div>:<div/>
                                }
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12 plan-footer">
                                All amounts shown are in AUD.
                            </div>
                        </div>
                    </div>
                </div>
                <div className="paid-invoice">
                    <h1 className="membership-invoice-header">Invoice</h1>
                    <MembershipInvoice membershipInvoice={this.state.subscriptionInvoices} />
                </div>
            </div>);
        }
        else if(this.state.subscriptionInvoices!==null) {
            return (
                <div>
                    <h1 className="membership-invoice-header">Invoice</h1>
                <MembershipInvoice membershipInvoice={this.state.subscriptionInvoices}/>
                </div>
            );
        }
        else{
            return(<div/>);
        }
    }
}