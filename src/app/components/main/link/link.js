import React, {Component} from 'react';

import './links.scss';

export default class Links extends Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.props.handleClick(this.props);
    }

    render() {
        return (<div className="navigation-link" onClick={this.handleClick.bind(this)}>
            {this.props.image ? <img className="left-nav-bar-icon" src={this.props.image}/> : this.props.icon}
            <span className="link-name">{this.props.name}</span>
        </div>);
    }
}