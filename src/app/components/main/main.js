import React, { Component } from 'react';
import { Router } from 'react-router-dom';
import ClickOutComponent from 'react-onclickout';


// custom components
import Routes from '../main/routes/routes';
import LeftNavBar from '../main/leftNavBar/leftNavBar';
import history from '../history';
import Header from '../header/header';

// custom css
import './main.scss';

export default class Main extends Component {

    constructor(props){
        super(props);
        this.state={showNavBar:true};
        this.toggleNavigationBar=this.toggleNavigationBar.bind(this);
        this.onClickOutside=this.onClickOutside.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(route){
        this.props.handleClick(route);
        if (this.state.showNavBar) {
            this.setState({showNavBar: false});
        }
    }

    toggleNavigationBar(e){
        let navBar=this.state.showNavBar;
        this.setState({showNavBar:!navBar});
        e.nativeEvent.stopImmediatePropagation();
    }

    onClickOutside() {
        if (this.state.showNavBar) {
            this.setState({showNavBar: false});
        }
    }

    render() {
        return (
            <div>
                <Header toggleNavigationBar={this.toggleNavigationBar.bind(this)}/>
                <Router history={history}>
                    <div>
                        <div className="hidden-sm-down">
                            <div className="left-nav-bar ">
                                <LeftNavBar handleClick={this.handleClick.bind(this)}/>
                            </div>
                        </div>
                        <div className="navbar-fixed-top hidden-md-up">
                            <ClickOutComponent onClickOut={this.onClickOutside}>
                                <div className={"left-nav-bar-mobile show-nav-bar-"+this.state.showNavBar}>
                                    <LeftNavBar handleClick={this.handleClick.bind(this)}/>
                                </div>
                            </ClickOutComponent>
                        </div>
                        <div className="container-fluid">
                            <div className="row">
                                <div className="display-component">
                                    <Routes auth={this.props.auth}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </Router>
            </div>
        );
    }
}
