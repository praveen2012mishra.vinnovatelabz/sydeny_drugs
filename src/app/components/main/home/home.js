import React, { Component } from 'react';

// customs scss
import './home.scss';

export default class Home extends Component {

    render() {
        return (
            <div className="col-md-12 col-sm-12 col-xs-12">
                <div className="welcome-header">
                    <h1>Welcome to the #SydneyDrugs Prescription Management Web App</h1>
                    <div className="content">
                        <div>You Can</div>
                        <div>
                            <div><span>1.</span><span>Add and remove payment methods</span></div>
                            <div><span>2.</span><span>Manage your membership</span></div>
                            <div><span>3.</span><span>View Invoices</span></div>
                            <div><span>4.</span><span>Track orders</span></div>
                            <div><span>5.</span><span>Chat with the pharmacist</span></div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


