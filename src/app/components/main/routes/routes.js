import React, { Component } from 'react';

/* Vendor components */
import { Route, Switch } from 'react-router-dom';

// Custom Components
import Home from '../home/home';
import Invoice from '../invoices/invoice';
import Membership from '../membership/membership';
import Payment from '../payment/payment';
import Shipping from '../shipping/shipping';
import Support from '../support/support';
import JoinMembership from '../membership/join-membership/join-membership';

export default class Routes extends Component {

    render() {
        return (
            <div>
                <Switch>
                    <Route  path="/app/dashboard" component={Home}/>
                    <Route  path="/app/invoice" component={Invoice}/>
                    <Route  exact path="/app/membership" render={()=><Membership auth={this.props.auth}/>}/>
                    <Route  exact path="/app/membership/join" component={JoinMembership}/>
                    <Route  path="/app/payment" component={Payment}/>
                    <Route  path="/app/shipping" component={Shipping}/>
                    <Route  path="/app/support" component={Support}/>
                </Switch>
            </div>
        );
    }
}