import React, { Component } from 'react';

export default class Login extends Component {

    componentDidMount(){
        if(this.props.auth){
            this.props.auth.login();
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.auth){
            nextProps.auth.login();
        }
    }

    render() {

        return (<div/>);
    }
}