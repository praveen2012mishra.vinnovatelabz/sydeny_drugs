import React, { Component } from 'react';
import { Router } from 'react-router-dom';
import IdleTimer from 'react-idle-timer';

/*Custom Components*/
import Routes from './components/routes/routes';
import history from './components/history';
import ModalSessionExpire from './components/modals/modal-session-expire';
import * as ConfigService from './config/config-service';
import AuthService from './components/auth/AuthService';

export default class App extends Component {

    constructor() {
        super();
        this.state = {auth: null,timeout:30*60*1000,showModal:false};
        this.onIdle=this.onIdle.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.hideModal=this.hideModal.bind(this);
        this.loadIntercom = this.loadIntercom.bind(this);
    }

    componentDidMount() {
        let authConfig = ConfigService.getAuthConfig();
        let auth = new AuthService(authConfig);
        let pathName = history.location.pathname;
        let splitPathName = pathName.split('&');
        if (splitPathName.length > 1) {
            let authResult = {
                accessToken: splitPathName[0].split('=')[1],
                idToken: splitPathName[1].split('=')[1]
            };
            auth._doAuthentication(authResult);
            history.push('/app/dashboard');
            let lock = auth.getLock();
            lock.getProfile(authResult.accessToken, (error, profile) => {
                if (error) {
                    console.log('Error loading the Profile', error);
                } else {
                    auth.setProfile(profile);
                    if (profile.user_metadata && profile.user_metadata.licenceAccepted) {
                        history.push('/app/dashboard');
                        this.loadIntercom(auth);
                    } else {
                        history.push('/eula');
                    }
                }
            });
        }
        else {
            if (auth.loggedIn()) {
                let pathName = history.location.pathname;
                if (pathName.includes('login') || pathName === '/') {
                    history.push('/app/dashboard');
                } else {
                    console.log("true");
                    history.push(history.location.pathname);
                }
                this.loadIntercom(auth);
            } else {
                history.push('/login');
            }
        }
        this.setState({auth: auth});
    }

    loadIntercom(auth){
        if (auth) {
            let response = ConfigService.getIntercomConfig();
            let user = auth.getProfile();
            let hash = CryptoJS.HmacSHA256(user.email, response.userIdentityKey);
            let hashInHex = CryptoJS.enc.Hex.stringify(hash);
            user.intercomHashCode = hashInHex;
            const evt = new CustomEvent('loadIntercom', {detail: user});
            window.dispatchEvent(evt);
        }
    }

    handleClick(route) {
        if (route.authorised) {
            history.push({pathname:route.to,state:'test'});
        } else {
            if (this.state.auth.loggedIn()) {
                history.push(route.to);
            } else {
                history.push('/login');
            }
        }
    }

    onIdle(){
        this.setState({showModal:true});
        localStorage.clear();
        Intercom('shutdown');
    }

    hideModal(){
        this.setState({showModal:false});
        history.push('/login');
    }

    render() {
        return (
            <div>
                <ModalSessionExpire showModal={this.state.showModal} closeSessionExpireModal={this.hideModal}/>
                <Router history={history}>
                    <Routes auth={this.state.auth} selectedRoute={this.state.selectedRoute}
                            handleClick={this.handleClick.bind(this)}/>
                </Router>
                <IdleTimer
                    idleAction={this.onIdle}
                    timeout={30*60*1000}>
                </IdleTimer>
            </div>);
    }
}