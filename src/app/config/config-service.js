import { config } from './clientConfig';

let clientConfig = process.env.NODE_ENV === "local" ? config.local : config.development;

export function getAuthConfig() {
    return clientConfig.auth0;
}

export function getIntercomConfig(){
    return clientConfig.intercom;
}

export function getStripeConfig(){
    return clientConfig.stripe;
}
