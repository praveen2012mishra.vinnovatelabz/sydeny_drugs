const config={
    local: {
        auth0: {
            clientId: "nBOUyrKQpuHbCq044tqUS1As2I76PKdm",
            domain: "sydneydrugsauth0.au.auth0.com",
            logo: "https://d26ylqsdsxp8yv.cloudfront.net/SDIcon.png"
        },
        zendesk:{
            host : "yeshelp.zendesk.com"
        },
        intercom:{
            userIdentityKey: 'YSs__ApJjeZWZFtlHwWIiUIQ0EE5DxoU7TcmV7PE'
        },
        stripe : {
            subscriptionStripePublishableKey : 'pk_test_i7MYsa3Sj1JCEL4x0T8o2XiG',
            paymentStripePublishableKey : 'pk_test_iTQ4wE4wHjR5RT3bPdvuPKjL',
        }
    },
    development: {
        auth0: {
            clientId: "2J0yDEIhTCcCkfcZ3ds3n511Ih8xG6TF",
            domain: "sydneydrugs.au.auth0.com",
            logo: "https://d26ylqsdsxp8yv.cloudfront.net/SDIcon.png"
        },
        zendesk:{
            host : "sydneydrugs.zendesk.com"
        },
        intercom:{
            userIdentityKey: 'YSs__ApJjeZWZFtlHwWIiUIQ0EE5DxoU7TcmV7PE'
        },
        stripe : {
            subscriptionStripePublishableKey : "pk_live_ed2ETk9ekmNAzIq5lIEg89Pc",
            paymentStripePublishableKey : "pk_live_u3fm1eVRXSAG3mmyj4eYSUbb"
        }
    },
    production: {
    }
};

export {config};