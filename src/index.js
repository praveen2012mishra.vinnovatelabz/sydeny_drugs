import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import injectTapEventPlugin from 'react-tap-event-plugin';
import fetchIntercept from 'fetch-intercept';

fetchIntercept.register({
    request: function (url, config) {
        if(config.headers){
            config.headers['auth-token']=localStorage.getItem('id_token');
        }else{
            config.headers ={'auth-token':localStorage.getItem('id_token')} ;
        }
        return [url, config];
    },
    requestError: function (error) {
        // Called when an error occured during another 'request' interceptor call
        return Promise.reject(error);
    },

    response: function (response) {
        // Modify the reponse object
        return response;
    },

    responseError: function (error) {
        // Handle an fetch error
        return Promise.reject(error);
    }
});

/*Custom Component*/
import App from './app/app';

// third party css
import 'bootstrap/dist/css/bootstrap.css';
import './assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css';

//custom SCSS
import './assets/styles/styles.scss';

injectTapEventPlugin();

render(
  <AppContainer>
   <App/>
  </AppContainer>,
  document.getElementById('app')
);

if (module.hot) {
  module.hot.accept('./app/app', () => {
    const NewApp = require('./app/app').default;
    render(
      <AppContainer>
        <NewApp />
      </AppContainer>,
      document.getElementById('app')
    );
  });
}

