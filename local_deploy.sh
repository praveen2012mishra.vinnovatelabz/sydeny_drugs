#!/bin/bash

HOST=HOST
USER=USER
PASS=PASS
PORT=PORT

localBuild(){

#Remove old build if present
rm -r deploy

#Create new build files
mkdir -p deploy
cp -r server package.json deploy/
cp remote_deploy.sh deploy/
cd deploy

#Compress the content to transfer
tar -cvf deploy.tar *

# Wait for 2 seconds
sleep 2

}

ftpTransfer(){

#Connect using SFTP
echo "sftping file ..."

lftp -p ${PORT} -u ${USER},${PASS} sftp://${HOST} <<EOF

mkdir -p temp_rx_sydneydrugs
cd temp_rx_sydneydrugs/
put deploy.tar deploy.tar
put remote_deploy.sh remote_deploy.sh

EOF

echo "******************* Done transfering files *******************"

# Wait for 2 seconds
sleep 2

}

cleanLocalBuild(){
echo "******************* Delete local build files *******************"
cd ..
rm -r deploy

}

sshDeployment(){

echo "******************* Deploying the transfered files *******************"

sshpass -f <(printf '%s\n' ${PASS}) ssh -p ${PORT} ${USER}@${HOST} <<EOF

mkdir -p rx.sydneydrugs.com
cd rx.sydneydrugs.com/
cp ../temp_rx_sydneydrugs/remote_deploy.sh .

chmod 777 remote_deploy.sh
./remote_deploy.sh

EOF

}

localBuild
ftpTransfer
cleanLocalBuild
sshDeployment

echo "done"
