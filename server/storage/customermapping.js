const Q = require('q');

const mysqlConfig = require('./config/mysqlConfig');

let service = {};

service.isCustomerExists = function (emailId) {
    let deferred = Q.defer();

    let qry = "SELECT * FROM customermapping WHERE email_id='" + emailId + "'";

    mysqlConfig.executeQuery(qry, function (err, result) {
        if (err) {
            deferred.reject(err);
        } else {
            deferred.resolve(result.length !== 0);
        }
    });


    return deferred.promise;
};


/* New Customer mapping for two stripe account */

service.getCustomer = function (emailId) {
    let deferred = Q.defer();

    let qry = "SELECT * FROM customermapping WHERE email_id='" + emailId + "'";



    mysqlConfig.executeQuery(qry, function (err, result) {
        if (err) {
            deferred.reject(err);
        }

        if (result.length === 0) {
            return deferred.reject({});
        }

        return deferred.resolve(result[0]);
    });

    return deferred.promise;
};

service.createCustomerMapping = function (subscriptionCustomerId, paymentCustomerId, emailId) {
    let deferred = Q.defer();

    let data = {subscription_customer_id: subscriptionCustomerId, payment_customer_id : paymentCustomerId, email_id: emailId};

    mysqlConfig.executePostQuery("INSERT INTO customermapping SET ?", data, function (err, result) {
        if (err) {
            deferred.reject(err);
        }
        return deferred.resolve(result);
    });

    return deferred.promise;
};

/* End new customer mapping for two stripe account */

module.exports = service;