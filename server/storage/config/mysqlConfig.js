const mysql = require('mysql');
const serverConfigService = require('../../services/serverConfigService');
const config = serverConfigService.get();
const SQLConfig = {};

const msqlConf = config.mysql;

let pool = mysql.createPool({
    connectionLimit: 25,
    host: msqlConf.host,
    user: msqlConf.un,
    password: msqlConf.pass,
    database: msqlConf.database,
    debug: false
});

/*
SQLConfig.getConnection = function () {
    return pool.getConnection(function (err, connection) {
        console.log("getConnection:", err, connection);
        return connection;
    });
};
*/

SQLConfig.executeQuery = function (query, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            return callback(err, null);
        }
        else if (connection) {
            connection.query(query, function (err, rows, fields) {
                connection.release();
                if (err) {
                    return callback(err, null);
                }
                return callback(null, rows);
            })
        }
        else {
            return callback(true, "No Connection");
        }
    });
};

SQLConfig.executePostQuery = function (query, data, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            return callback(err, null);
        }
        else if (connection) {
            connection.query(query, data, function (err, rows, fields) {
                connection.release();
                if (err) {
                    return callback(err, null);
                }
                return callback(null, rows);
            })
        }
        else {
            return callback(true, "No Connection");
        }
    });
};



SQLConfig.getResult = function (query, callback) {
    executeQuery(query, function (err, rows) {
        if (!err) {
            callback(null, rows);
        }
        else {
            callback(true, err);
        }
    });
};

module.exports = SQLConfig;