const winston = require('winston');
winston.emitErrs = true;


const logger = new winston.Logger({
    transports: [
        new winston.transports.File({
            level: 'debug',
            color : 'blue',
            filename: 'rx.log',
            handleExceptions: true,
            json: true,
            maxsize: 5242880, //5MB
            maxFiles: 5,
            colorize: true,
        })
    ],
    exitOnError: false
});


module.exports = logger;