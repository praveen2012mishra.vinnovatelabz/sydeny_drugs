const Q = require('q');
const Auth0 = require('auth0');
const serverConfigService = require('../services/serverConfigService');
const config = serverConfigService.get();
const AuthenticationClient = Auth0.AuthenticationClient;
const apiClient = new AuthenticationClient({
    domain: config.auth0.domain,
    clientId: config.auth0.clientId
});


let auth0Service = {};

auth0Service.getProfileFromToken = function (token) {
    let deferred = Q.defer();
    apiClient.tokens.getInfo(token, function (err, profile) {
        if (err) {
            deferred.reject({message: 'Token is invalid'});
        } else {
            if(profile === undefined || profile === null || profile === "Unauthorized"){
                deferred.reject({message: 'Token is invalid'});
            }else {
                deferred.resolve(profile);
            }
        }
    });
    return deferred.promise;
};

auth0Service.acceptTermsAndConditions = function (curUser, token) {
    let deferred = Q.defer();
    const request = require("request");

    const options = { method: 'PATCH',
        url: 'https://' + config.auth0.domain + '/api/v2/users/' + curUser.user_id,
        headers:
            { 'content-type': 'application/json',
                authorization: 'Bearer ' + token },
        body: { user_metadata: { licenceAccepted: true }},
        json: true };

    request(options, function (error, response, body) {
        if (error) {
            deferred.reject(error);
        } else {
            deferred.resolve(body);
        }
    });
    return deferred.promise;
};

module.exports = auth0Service;
