const Q = require('q');
const serverConfigService = require('../services/serverConfigService');
const config = serverConfigService.get();
const stripe = require("stripe")(config.stripe.paymentStripeSecretKey);

const logger = require('../logger/logger');

const stripePaymentService = {};

/*Customer APIs*/
stripePaymentService.createCustomer = function (emailId) {
    let deferred = Q.defer();

    stripe.customers.create({
        email: emailId,
    }).then(function (response) {
        deferred.resolve(response);
    }, function (err) {
        logger.error(err);
        deferred.reject(err);
    });

    return deferred.promise;
};


stripePaymentService.getCustomer = function (customerId) {
    let deferred = Q.defer();

    stripe.customers.retrieve(customerId).then(function (response) {
        deferred.resolve(response);
    }, function (err) {
        logger.error(err);
        deferred.reject(err);
    });

    return deferred.promise;
};

/*End of Customer APIs*/



/*Card APIs*/

stripePaymentService.addCardToStripe = function (token, customerId) {
    let deferred = Q.defer();
    stripePaymentService.validateDuplicateCard(token, customerId).then(function (response) {
        if (!response) {
            stripe.customers.createSource(customerId, {source: token},
                function (err, card) {
                    if (card) {
                        deferred.resolve(card);
                    }
                    if (err) {
                        logger.error(err);
                        deferred.reject(err);
                    }
                }
            );
        }
    }, function (error) {
        logger.error(error);
        deferred.reject(error);
    });

    return deferred.promise;
};

stripePaymentService.getCardFromToken = function (token) {
    let deferred = Q.defer();
    stripe.tokens.retrieve(token, function (err, token) {
            if (token) {
                let card = token.card;
                deferred.resolve(card);
            } else {
                logger.error(err);
                deferred.reject(err);
            }
        }
    );
    return deferred.promise;
};


stripePaymentService.getAllCustomerCards = function (customerId) {
    let deferred = Q.defer();
    let customerCards = [];
    stripe.customers.retrieve(customerId, function (err, customer) {
        if (err) {
            logger.error(err);
            deferred.reject(err);
        }

        if (customer.sources === undefined || customer.sources.data === undefined) {
            return deferred.resolve(customerCards);
        }

        let accountCollection = customer.sources.data;
        let defaultCardId = customer.default_source;

        if (accountCollection.length === 0) {
            deferred.resolve(customerCards);
        } else {
            accountCollection.forEach(function (card, index) {
                if (defaultCardId === card.id)
                    card.defaultCard = true;
                customerCards.push(card);
                if (accountCollection.length - 1 === index)
                    deferred.resolve(customerCards);
            });
        }

    });
    return deferred.promise;
};



stripePaymentService.validateCard = function (cardId, customerId) {
    let deferred = Q.defer();
    stripe.customers.retrieveCard(
        customerId, cardId, function (err, card) {
            if (err) {
                logger.error(err);
                deferred.reject({'error': 'Please provide valid card'});
            } else
                deferred.resolve(card);
        });
    return deferred.promise;
};


stripePaymentService.validateDuplicateCard = function (token, customerId) {
    let deferred = Q.defer();

    let accountCollection;

    stripePaymentService.getCardFromToken(token).then(function (card) {
        stripe.customers.retrieve(customerId, function (err, customer) {
                if (err) {
                    logger.error(err);
                    deferred.reject(err);
                } else
                    accountCollection = customer.sources.data;
                if (accountCollection.length === 0) {
                    deferred.resolve(false);
                } else {
                    accountCollection.forEach(function (account, index) {
                        if (account.fingerprint === card.fingerprint) {
                            deferred.reject({'error': 'Card already exists'});
                        }
                        if (accountCollection.length - 1 === index) {
                            deferred.resolve(false);
                        }
                    });
                }

            }
        );
    }, function (error) {
        logger.error(error);
        deferred.reject(error);
    });

    return deferred.promise;
};


stripePaymentService.getDefaultCardId = function (customerId) {
    let deferred = Q.defer();

    stripe.customers.retrieve(customerId).then(function (customer, err) {
        if (err) {
            logger.error(err);
            deferred.reject(err);
        }
        else {
            let cardId = customer.default_source;
            deferred.resolve(cardId);
        }
    });

    return deferred.promise;
};

stripePaymentService.deleteCardOnStripe = function (customerId, cardId) {
    let deferred = Q.defer();

    stripe.customers.deleteCard(customerId, cardId,
        function (err, confirmation) {
            if (err) {
                logger.error(err);
                deferred.reject(err);
            }
            else
                deferred.resolve(confirmation.deleted);
        }
    );

    return deferred.promise;
};

/*End of Card APIs*/


/*Charge charge invoices */
stripePaymentService.getCharges = function (customerId) {
    let deferred = Q.defer();

    stripe.charges.list(
        {limit:100, customer: customerId},
        function (err, charges) {
            if (err) {
                logger.error(err);
                return deferred.reject(err);
            }
            else
                return deferred.resolve(charges);
        }
    );

    return deferred.promise;
};
/*End of Charge Invoices*/


/* Create charge services */

stripePaymentService.makeCharge = function (customerId, charge) {
    let deferred = Q.defer();

    stripe.charges.create({
        amount: charge.amount,
        currency: "aud",
        source: charge.cardId, // obtained with Stripe.js
        customer: customerId,
        description: (charge.description) ? charge.description : "",
    }, function (err, result) {
        if (err) {
            logger.error(err);
            return deferred.reject(err);
        }
        else
            return deferred.resolve(result);
    });

    return deferred.promise;
};


/* End of Create charge services */

/* getting pending invoices */

stripePaymentService.getInvoices = function (customerId) {
    let deferred = Q.defer();

    stripe.invoices.list(
        {limit:100, customer: customerId},
        function (err, invoices) {
            if (err) {
                logger.error(err);
                return deferred.reject(err);
            }
            else
                return deferred.resolve(invoices);
        }
    );

    return deferred.promise;
};


/* Making payment for pending invoices */
stripePaymentService.makePayment = function (cardId, invoiceId) {
    let deferred = Q.defer();

    stripe.invoices.pay(invoiceId,
        {source: cardId},
        function (err, result) {
            if (err) {
                logger.error(err);
                return deferred.reject(err);
            }
            else
                return deferred.resolve(result);
        }
    );

    return deferred.promise;
};



module.exports = stripePaymentService;