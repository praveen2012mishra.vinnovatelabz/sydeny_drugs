const Q = require('q');
const serverConfigService = require('../services/serverConfigService');
const config = serverConfigService.get();

const stripe = require("stripe")(config.stripe.subscriptionStripeSecretKey);

const logger = require('../logger/logger');

const stripeSubscriptionService = {};


/*Customer APIs*/
stripeSubscriptionService.createCustomer = function (emailId) {
    let deferred = Q.defer();

    stripe.customers.create({
        email: emailId,
    }).then(function (response) {
        deferred.resolve(response);
    }, function (err) {
        logger.error(err);
        deferred.reject(err);
    });

    return deferred.promise;
};


stripeSubscriptionService.getCustomer = function (customerId) {
    let deferred = Q.defer();

    stripe.customers.retrieve(customerId).then(function (response) {
        deferred.resolve(response);
    }, function (err) {
        logger.error(err);
        deferred.reject(err);
    });

    return deferred.promise;
};

/*End of Customer APIs*/


/*Subscription APIs*/
stripeSubscriptionService.createSubscription = function (customerId, plan, token) {
    let deferred = Q.defer();

    stripe.subscriptions.create({
        customer: customerId,
        items: [
            {
                plan: plan
            }
        ],
        source : token,
    }).then(function (response) {
        deferred.resolve(response);
    }, function (err) {
        logger.error(err);
        deferred.reject(err);
    });

    return deferred.promise;
};


stripeSubscriptionService.getSubscriptions = function (customerId) {
    let deferred = Q.defer();

    stripe.subscriptions.list(
        {customer: customerId}).then(function (response) {
        deferred.resolve(response);
    }, function (err) {
        logger.error(err);
        deferred.reject(err);
    });

    return deferred.promise;
};


stripeSubscriptionService.updateSubscription = function (subscription, planId) {
    let deferred = Q.defer();
    let item_id = subscription.data[0].items.data[0].id;
    stripe.subscriptions.update(subscription.data[0].id, {
        'items': [{id: item_id, plan: planId}],
        prorate: true
    }).then(function (response) {
        deferred.resolve(response);
    }, function (err) {
        logger.error(err);
        deferred.reject(err);
    });

    return deferred.promise;
};


/*End of Subscription APIs*/

/*Invoices*/
stripeSubscriptionService.getInvoices = function (customerId) {
    let deferred = Q.defer();

    stripe.invoices.list(
        {limit:100, customer: customerId},
        function (err, invoices) {
            if (err) {
                logger.error(err);
                return deferred.reject(err);
            }
            else
                return deferred.resolve(invoices);
        }
    );

    return deferred.promise;
};
/*End of Invoices*/


module.exports = stripeSubscriptionService;



