const Q = require('q');
let config = require('../utils/clientconfig.js')[process.env.NODE_ENV || 'local'];

const clientConfigService = {};

clientConfigService.get = function () {
    return config;
};

module.exports = clientConfigService;