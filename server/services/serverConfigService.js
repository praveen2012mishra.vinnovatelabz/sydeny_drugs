const Q = require('q');
let config = require('../utils/serverconfig.js')[process.env.NODE_ENV || 'local'];

const serverConfigService = {};

serverConfigService.get = function () {
    return config;
};

module.exports = serverConfigService;