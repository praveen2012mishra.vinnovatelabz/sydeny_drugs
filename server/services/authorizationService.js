let Q = require('q');

let auth0Service = require('../services/auth0Service.js');

let authorizationService = {};

authorizationService.validateToken = function (req, token) {
    let deferred = Q.defer();
    auth0Service.getProfileFromToken(token).then(function (profile) {
        if(!profile){
            deferred.reject(error);
        }
        deferred.resolve(profile);
    },function (error) {
        deferred.reject(error);
    });

    return deferred.promise;
};

authorizationService.acceptTermsAndConditions = function (curUser, token) {
    let deferred = Q.defer();
    auth0Service.acceptTermsAndConditions(curUser, token).then(function (user) {
        deferred.resolve(user);
    }, function (error) {
        deferred.reject(error);
    });
    return deferred.promise;
};


module.exports = authorizationService;
