const Q = require('q');
const serverConfigService = require('../services/serverConfigService');
const config = serverConfigService.get();
const stripe = require("stripe")(config.stripe.subscriptionStripeSecretKey);

const logger = require('../logger/logger');

const stripeService = {};

/*Customer APIs*/
stripeService.createCustomer = function (emailId) {
    let deferred = Q.defer();

    stripe.customers.create({
        email: emailId,
    }).then(function (response) {
        deferred.resolve(response);
    }, function (err) {
        logger.error(err);
        deferred.reject(err);
    });

    return deferred.promise;
};


stripeService.getCustomer = function (customerId) {
    let deferred = Q.defer();

    stripe.customers.retrieve(customerId).then(function (response) {
        deferred.resolve(response);
    }, function (err) {
        logger.error(err);
        deferred.reject(err);
    });

    return deferred.promise;
};

/*End of Customer APIs*/


/*Subscription APIs*/
stripeService.createSubscription = function (customerId, plan) {
    let deferred = Q.defer();

    let metadata = {};
    let aYearFromNow = new Date();
    aYearFromNow.setFullYear(aYearFromNow.getFullYear() + 1);
    metadata.expiryOn = aYearFromNow;

    stripe.subscriptions.create({
        customer: customerId,
        items: [
            {
                plan: plan
            },
        ],
        metadata: metadata
    }).then(function (response) {
        deferred.resolve(response);
    }, function (err) {
        logger.error(err);
        deferred.reject(err);
    });

    return deferred.promise;
};


stripeService.getSubscriptions = function (customerId) {
    let deferred = Q.defer();

    stripe.subscriptions.list(
        {customer: customerId}).then(function (response) {
        deferred.resolve(response);
    }, function (err) {
        logger.error(err);
        deferred.reject(err);
    });

    return deferred.promise;
};

stripeService.cancelSubscription = function (subscriptionId) {
    let deferred = Q.defer();

    stripe.subscriptions.del(subscriptionId).then(function (response) {
        deferred.resolve(response);
    }, function (err) {
        logger.error(err);
        deferred.reject(err);
    });

    return deferred.promise;
};


stripeService.cancelAllSubscriptions = function (customer) {
    let deferred = Q.defer();

    if (customer.subscriptions.data !== undefined) {
        let count = 0;
        let subscriptionCount = customer.subscriptions.data.length;
        for (index in customer.subscriptions.data) {
            let eachSubscription = customer.subscriptions.data[index];
            stripeService.cancelSubscription(eachSubscription.id).then(function (response) {
                count++;
                if (count === subscriptionCount) {
                    return deferred.resolve({});
                }
            }, function (err) {
                logger.error(err);
                return deferred.reject(err);
            });
        }
    } else {
        return deferred.resolve({});
    }

    return deferred.promise;
};

stripeService.updateSubscription = function (subscription, planId) {
    let deferred = Q.defer();
    let item_id = subscription.data[0].items.data[0].id;
    stripe.subscriptions.update(subscription.data[0].id, {
        'items': [{id: item_id, plan: planId}],
        prorate: true
    }).then(function (response) {
        deferred.resolve(response);
    }, function (err) {
        logger.error(err);
        deferred.reject(err);
    });

    return deferred.promise;
};


/*End of Subscription APIs*/

/*Card APIs*/

stripeService.addCardToStripe = function (token, customerId) {
    let deferred = Q.defer();
    stripeService.validateDuplicateCard(token, customerId).then(function (response) {
        if (!response) {
            stripe.customers.createSource(customerId, {source: token},
                function (err, card) {
                    if (card) {
                        deferred.resolve(card);
                    }
                    if (err) {
                        logger.error(err);
                        deferred.reject(err);
                    }
                }
            );
        }
    }, function (error) {
        logger.error(error);
        deferred.reject(error);
    });

    return deferred.promise;
};

stripeService.getCardFromToken = function (token) {
    let deferred = Q.defer();
    stripe.tokens.retrieve(token, function (err, token) {
            if (token) {
                let card = token.card;
                deferred.resolve(card);
            } else {
                logger.error(err);
                deferred.reject(err);
            }
        }
    );
    return deferred.promise;
};


stripeService.getAllCustomerCards = function (customerId) {
    let deferred = Q.defer();
    let customerCards = [];
    stripe.customers.retrieve(customerId, function (err, customer) {
        if (err) {
            logger.error(err);
            deferred.reject(err);
        }

        if (customer.sources === undefined || customer.sources.data === undefined) {
            return deferred.resolve(customerCards);
        }

        let accountCollection = customer.sources.data;
        let defaultCardId = customer.default_source;

        if (accountCollection.length === 0) {
            deferred.resolve(customerCards);
        } else {
            accountCollection.forEach(function (card, index) {
                if (defaultCardId === card.id)
                    card.defaultCard = true;
                customerCards.push(card);
                if (accountCollection.length - 1 === index)
                    deferred.resolve(customerCards);
            });
        }

    });
    return deferred.promise;
};

stripeService.updadeCardOnStripe = function (customerId, customerCard) {
    let deferred = Q.defer();
    stripe.customers.updateCard(
        customerId, customerCard.cardId,
        {name: customerCard.cardHolderName, exp_month: customerCard.expiryMonth, exp_year: customerCard.expiryYear},
        function (err, card) {
            if (card) {
                deferred.resolve(card);
            }
            else if (err) {
                logger.error(err);
                deferred.reject(err);
            }
        });
    return deferred.promise;
};

stripeService.setDefaultCard = function (cardId, customerId) {
    let deferred = Q.defer();
    stripeService.validateCard(cardId, customerId).then(function (response) {
        stripe.customers.update(customerId, {
            'default_source': cardId
        }, function (err, customer) {
            if (err) {
                logger.error(err);
                deferred.reject(err);
            }
            if (customer)
                deferred.resolve(customer);
        });
    }, function (error) {
        logger.error(error);
        deferred.reject(error);
    });
    return deferred.promise;
};

stripeService.validateCard = function (cardId, customerId) {
    let deferred = Q.defer();
    stripe.customers.retrieveCard(
        customerId, cardId, function (err, card) {
            if (err) {
                logger.error(err);
                deferred.reject({'error': 'Please provide valid card'});
            } else
                deferred.resolve(card);
        });
    return deferred.promise;
};


stripeService.validateDuplicateCard = function (token, customerId) {
    let deferred = Q.defer();

    let accountCollection;

    stripeService.getCardFromToken(token).then(function (card) {
        stripe.customers.retrieve(customerId, function (err, customer) {
                if (err) {
                    logger.error(err);
                    deferred.reject(err);
                } else
                    accountCollection = customer.sources.data;
                if (accountCollection.length === 0) {
                    deferred.resolve(false);
                } else {
                    accountCollection.forEach(function (account, index) {
                        if (account.fingerprint === card.fingerprint) {
                            deferred.reject({'error': 'Card already exists'});
                        }
                        if (accountCollection.length - 1 === index) {
                            deferred.resolve(false);
                        }
                    });
                }

            }
        );
    }, function (error) {
        logger.error(error);
        deferred.reject(error);
    });

    return deferred.promise;
};


stripeService.getDefaultCardId = function (customerId) {
    let deferred = Q.defer();

    stripe.customers.retrieve(customerId).then(function (customer, err) {
        if (err) {
            logger.error(err);
            deferred.reject(err);
        }
        else {
            let cardId = customer.default_source;
            deferred.resolve(cardId);
        }
    });

    return deferred.promise;
};

stripeService.deleteCardOnStripe = function (customerId, cardId) {
    let deferred = Q.defer();

    stripe.customers.deleteCard(customerId, cardId,
        function (err, confirmation) {
            if (err) {
                logger.error(err);
                deferred.reject(err);
            }
            else
                deferred.resolve(confirmation.deleted);
        }
    );

    return deferred.promise;
};

/*End of Card APIs*/


/*Invoices*/
stripeService.getInvoices = function (customerId) {
    let deferred = Q.defer();

    stripe.invoices.list(
        {limit:100, customer: customerId},
        function (err, invoices) {
            if (err) {
                logger.error(err);
                return deferred.reject(err);
            }
            else
                return deferred.resolve(invoices);
        }
    );

    return deferred.promise;
};
/*End of Invoices*/



module.exports = stripeService;