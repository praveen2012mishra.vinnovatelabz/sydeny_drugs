const express = require('express');
const router = express.Router();

const authorizationService = require('../services/authorizationService.js');
const clientConfigService = require('../services/clientConfigService');

router.get('/iConfig', getIntercomConfig);
router.get('/stripeConfig', getStripeConfig);
router.get('/acceptTermsAndConditions', acceptTermsAndConditions);

function getIntercomConfig(req, res) {
    res.json(clientConfigService.get().intercom);
}

function getStripeConfig(req, res) {
    res.json(clientConfigService.get().stripe);
}

function acceptTermsAndConditions(req, res) {
    authorizationService.acceptTermsAndConditions(req.currentUser, req.authToken).then(function (user) {
        res.status(200).send(user);
    }, function (error) {
        res.status(401).send('UNAUTHORIZED');
    });
}

module.exports = router;