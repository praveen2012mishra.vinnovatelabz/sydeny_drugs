const express = require('express');
const router = express.Router();
const async = require('promise-async');

const stripeSubscriptionService = require('../services/stripeSubscriptionServices');
const stripePaymentService = require('../services/stripePaymentServices');

const logger = require('../logger/logger');

//routes

/* Subscription routes API */
router.post('/subscription/create', createSubscriptionAPI);
router.post('/subscription/change', changeSubscriptionsAPI);
router.get('/subscription/get', getSubscriptionsAPI);
router.get('/subscription/invoices', getSubscriptionsInvoicesAPI);

/* Payment routes API */
router.get('/card/get', getCardsAPI);
router.post('/card/add', addCardAPI);
router.post('/card/delete', deleteCardAPI);
router.get('/charges', getChargesAPI);
router.post('/makecharge', makeChargeAPI);
router.get('/pendinginvoices', getPendingInvoicesAPI);
router.post('/makepayment', makePaymentAPI);

module.exports = router;


/* Services for subscription stripe account  */

function createSubscriptionAPI(req, res) {
    async.waterfall([
            function (callback) {
                getSubscriptions(req.currentUser.subscriptionStripeCustomer.id, callback);
            },
            function (subscription, callback) {
                createSubscription(req.currentUser.subscriptionStripeCustomer.id, req.body.planId, req.body.token, subscription, callback);
            }],
// optional callback
        function (err, result) {
            // results is now equal to ['one', 'two']
            if (err) {
                logger.error(err);
                return res.sendStatus(500);
            }
            return res.json(result);
        });
}

function getSubscriptionsAPI(req, res) {

    async.waterfall([
        function (callback) {
            logger.debug(req);
            getSubscriptions(req.currentUser.subscriptionStripeCustomer.id, callback);
        }
    ], function (err, result) {
        if (err) {
            logger.error(err);
            res.sendStatus(500);
        }
        res.json(result);
    });
}


function changeSubscriptionsAPI(req, res) {
    async.waterfall([
        function (callback) {
            logger.debug(req);
            updateSubscription(req.currentUser.subscriptionStripeCustomer.subscriptions, req.body.planId, callback);
        }
    ], function (err, result) {
        if (err) {
            logger.error(err);
            res.sendStatus(500);
        }
        res.json(result);
    });
}

function getSubscriptionsInvoicesAPI(req, res) {
    async.waterfall([
        function (callback) {
            logger.debug(req);
            getSubscriptionsInvoices(req.currentUser.subscriptionStripeCustomer.id, callback);
        }
    ], function (err, result) {
        if (err) {
            logger.error(err);
            res.sendStatus(500);
        }
        res.json(result);
    });
}


function createSubscription(customerId, planId, token, subscription, callback) {
    if (subscription === undefined || subscription.data === undefined || subscription.data.length === 0) {
        stripeSubscriptionService.createSubscription(customerId, planId, token).then(function (response) {
            callback(null, response);
        }, function (err) {
            logger.error(err);
            callback(err);
        });
    } else {
        callback(null);
    }
}

function getSubscriptions(customerId, callback) {
    stripeSubscriptionService.getSubscriptions(customerId).then(function (subscription) {
        callback(null, subscription);
    }, function (err) {
        logger.error(err);
        callback(err);
    });
}


function updateSubscription(subscriptions, planId, callback) {
    if (subscriptions.data !== undefined && subscriptions.data.length === 1) {
        stripeSubscriptionService.updateSubscription(subscriptions, planId).then(function (response) {
            callback(null, response);
        }, function (err) {
            logger.error(err);
            callback(err);
        });
    } else {
        callback(null);
    }

}

function getSubscriptionsInvoices(customerId, callback) {
    stripeSubscriptionService.getInvoices(customerId).then(function (invoices) {

        callback(null, invoices);

    }, function (err) {
        logger.error(err);
        callback(err);
    });
}

/* End Services for subscription stripe account  */


/* Services for payment stripe account */

function getCardsAPI(req, res) {
    async.waterfall([
        function (callback) {
            logger.debug(req);
            getCards(req.currentUser.paymentStripeCustomer.id, callback);
        }
    ], function (err, result) {
        if (err) {
            logger.error(err);
            res.sendStatus(500);
        }
        res.json(result);
    });
}


function addCardAPI(req, res) {
    async.waterfall([
        function (callback) {
            logger.debug(req);
            addCard(req.currentUser.paymentStripeCustomer.id, req.body.token, callback);
        }
    ], function (err, result) {
        if (err) {
            logger.error(err);
            return res.sendStatus(500);
        }

        return res.json(result);
    });
}


function deleteCardAPI(req, res) {
    async.waterfall([
        function (callback) {
            logger.debug(req);
            deleteCard(req.currentUser.paymentStripeCustomer.id, req.body.cardId, callback);
        }], function (err, result) {
        if (err) {
            logger.error(err);
            return res.sendStatus(500);
        }

        return res.json(result);
    });
}


function getChargesAPI(req, res){

    async.waterfall([
        function (callback) {
            logger.debug(req);
            getCharges(req.currentUser.paymentStripeCustomer.id, callback);
        }
    ], function (err, result) {
        if (err) {
            logger.error(err);
            res.sendStatus(500);
        }
        res.json(result);
    });
}

function makeChargeAPI(req, res) {
    async.waterfall([
        function (callback) {
            logger.debug(req);
            makeCharge(req.currentUser.paymentStripeCustomer.id, req.body.charge, callback);
        }
    ], function (err, result) {
        if (err) {
            logger.error(err);
            res.sendStatus(500);
        }
        res.json(result);
    });
}

function getPendingInvoicesAPI(req, res) {
    async.waterfall([
        function (callback) {
            logger.debug(req);
            getPendingInvoices(req.currentUser.paymentStripeCustomer.id, callback);
        }
    ], function (err, result) {
        if (err) {
            logger.error(err);
            res.sendStatus(500);
        }
        res.json(result);
    });
}

function makePaymentAPI(req, res) {
    async.waterfall([
        function (callback) {
            logger.debug(req);
            makePayment(req.body.cardId, req.body.invoiceId, callback);
        }
    ], function (err, result) {
        if (err) {
            logger.error(err);
            res.sendStatus(500);
        }
        res.json(result);
    });
}


function getCards(customerId, callback) {
    stripePaymentService.getAllCustomerCards(customerId).then(function (response) {
        callback(null, response);
    }, function (err) {
        logger.error(err);
        callback(err);
    });
}

function addCard(customerId, token, callback) {
    stripePaymentService.addCardToStripe(token, customerId).then(function (response) {
        callback(null, response);
    }, function (err) {
        logger.error(err);
        callback(err);
    });
}


function deleteCard(customerId, cardId, callback) {
    stripePaymentService.deleteCardOnStripe(customerId, cardId).then(function (response) {
        callback(null, response);
    }, function (err) {
        logger.error(err);
        callback(err);
    });
}

function getCharges(customerId, callback) {
    stripePaymentService.getCharges(customerId).then(function (response) {
        callback(null, response);
    }, function (err) {
        logger.error(err);
        callback(err);
    });
}

function makeCharge(customerId, charge, callback) {
    stripePaymentService.makeCharge(customerId, charge).then(function (response) {
        callback(null, response);
    }, function (err) {
        logger.error(err);
        callback(err);
    });
}

function getPendingInvoices(customerId, callback) {
    stripePaymentService.getInvoices(customerId).then(function (response) {
        let pendingInvoices = filterPendingInvoices(response);
        callback(null, pendingInvoices);
    }, function (err) {
        logger.error(err);
        callback(err);
    });
}

function filterPendingInvoices(response) {
    let pendingInvoices = [];
    response.data.map(function (invoice) {
        if (!invoice.paid) {
            pendingInvoices.push(invoice);
        }
    });
    return pendingInvoices;
}


function makePayment(cardId, invoiceId, callback) {
    stripePaymentService.makePayment(cardId, invoiceId).then(function (response) {
        callback(null, response);
    }, function (err) {
        logger.error(err);
        callback(err);
    });
}

/* End services for payment stripe account */



