const express = require('express');
const router = express.Router();

const service = require('../services/clientConfigService');

//routes
router.get('/get', get);

function get(req, res) {
    res.json(service.get());
}

module.exports = router;