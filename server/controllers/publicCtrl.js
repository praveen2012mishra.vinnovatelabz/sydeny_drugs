const express = require('express');
const router = express.Router();
const async = require('promise-async');
/*const assert = require('chai').assert;*/

const authorizationService = require('../services/authorizationService.js');
const clientConfigService = require('../services/clientConfigService');

router.get('/validate', validateAPI);
router.get('/clientConfig', clientConfig);

function clientConfig(req, res) {
    res.json(clientConfigService.get());
}

function validateAPI(req, res){
    authorizationService.validateToken(req, token).then(function (profile) {
        if(profile){
            res.status(200);
        }else{
            res.send('401', 'UNAUTHORIZED');
        }
    },function (error) {
        res.send('401', 'UNAUTHORIZED');
    });
}

module.exports = router;