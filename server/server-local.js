const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const session = require('express-session');

const tokenFilter = require('./filters/tokenFilter.js');
const customerFilter = require('./filters/customerFilter.js');

app.set('port', 9337);

app.use(express.static(__dirname + '/dist'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use('/api', tokenFilter.validator);
app.use('/api', customerFilter.mapUserToCustomer);
/*Setting session for app - Can replace string SECRET by any random and strong secret*/
app.use(session({secret: 'SECRET', resave: false, saveUninitialized: true}));

/*app.get([ '/' ], function (request, response) {
    console.log(request.session.subDomain);
    response.sendFile(path.join(__dirname + '/public/static/index.html'));
});*/

app.use(['/public/'], require('./controllers/publicCtrl'));
app.use(['/api/user'], require('./controllers/user'));
app.use(['/api/util'], require('./controllers/utilCtrl'));

var serveStatic = require('serve-static');

//var server = connect();

app.use(serveStatic(__dirname + '/dist'));
console.log(__dirname);

app.listen(app.get('port'),function(){
    console.log('Node app is running on port', app.get('port'));
});

var livereload = require('livereload');
var lrserver = livereload.createServer();
lrserver.watch(__dirname + "/dist");
