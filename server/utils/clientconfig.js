module.exports = {
    local: {
        auth0: {
            clientId: "nBOUyrKQpuHbCq044tqUS1As2I76PKdm",
            domain: "sydneydrugsauth0.au.auth0.com",
            logo: "https://d26ylqsdsxp8yv.cloudfront.net/SDIcon.png"
        },
        zendesk:{
            host : "yeshelp.zendesk.com"
        },
        intercom:{
            userIdentityKey: 'YSs__ApJjeZWZFtlHwWIiUIQ0EE5DxoU7TcmV7PE'
        },
        stripe : {
            subscriptionStripePublishableKey : 'pk_test_i7MYsa3Sj1JCEL4x0T8o2XiG',
            paymentStripePublishableKey : 'pk_test_iTQ4wE4wHjR5RT3bPdvuPKjL',
        }
    },
    development: {
        auth0: {
            clientId: process.env.auth0ClientId,
            domain: process.env.auth0Domain,
            logo: "https://d26ylqsdsxp8yv.cloudfront.net/SDIcon.png"
        },
        zendesk:{
            host : "sydneydrugs.zendesk.com"
        },
        intercom:{
            userIdentityKey: 'YSs__ApJjeZWZFtlHwWIiUIQ0EE5DxoU7TcmV7PE'
        },
        stripe : {
            subscriptionStripePublishableKey : process.env.stripe_Publishable_key,
            paymentStripePublishableKey : process.env.stripe_Publishable_key
        }
    },
    production: {
    }
};