const async = require('promise-async');

const customermapping = require('../storage/customermapping');
const stripeSubscriptionService = require('../services/stripeSubscriptionServices');
const stripePaymentService = require('../services/stripePaymentServices');

const logger = require('../logger/logger');

const customerFilter = {};

customerFilter.mapUserToCustomer = function (req, res, next) {
    logger.debug(req);
    createCustomerIfNotExists(req, function (err, response) {
        if(err) {
            logger.error(err);
            res.send('500', 'Error Mapping Customer to User');
        }
        else{
            req.currentUser.subscriptionStripeCustomer = response.subscriptionStripeCustomer;
            req.currentUser.paymentStripeCustomer = response.paymentStripeCustomer;
            return next();
        }
    });
};


function createCustomerIfNotExists(req, callback) {
    async.waterfall([
            function (callbackOuter) {
                logger.debug(req);
                isCustomerExists(req.currentUser.email, callbackOuter);
            },
            function (customerExists, callbackOuter) {
                if (customerExists) {
                    async.waterfall([
                        function (callbackInner) {
                            getCustomer(req.currentUser.email, callbackInner);
                        },
                        function (customer, callbackInner) {
                            getCustomersFromStripe(customer, callbackInner);
                        }

                    ], function (err, result) {
                        if (err) {
                            logger.error(err);
                            callbackOuter(err);
                        }
                        callbackOuter(null, result);
                    });
                } else {
                    async.waterfall([
                        function (callbackInner) {
                            createCustomersOnStripe(req.currentUser.email, callbackInner);
                        },
                        function (customer, callbackInner) {
                            createCustomerMapping(customer, req.currentUser.email, callbackInner);
                        },
                        function (customer, callbackInner) {
                            getCustomersFromStripe(customer, callbackInner);
                        }
                    ], function (err, result) {
                        if (err) {
                            logger.error(err);
                            callbackOuter(err);
                        }
                        callbackOuter(null, result);
                    });
                }
            }
        ],
// optional callback
        function (err, result) {
            // results is now equal to ['one', 'two']
            if (err) {
                logger.error(err);
                callback(err);
            }

            callback(null, result);
        });
}

function createCustomersOnStripe(email, callback) {
    let customer = {};
    async.waterfall([
        function (callbackInner) {
            createCustomerForSubscription(email, callbackInner);
        },
        function (subscriptionStripeCustomer, callbackInner) {
            customer.subscription_customer_id = subscriptionStripeCustomer.id;
            createCustomerForPayment(email, callbackInner);
        }
    ], function (err, paymentStripeCustomer) {
        if (err) {
            logger.error(err);
            callback(err);
        }
        customer.payment_customer_id = paymentStripeCustomer.id;
        callback(null, customer);
    });
}

function getCustomersFromStripe(req, callback) {
    let stripeCustomer = {};
    async.waterfall([
        function (callbackInner) {
            getCustomerForSubscription(req.subscription_customer_id, callbackInner);
        },
        function (subscriptionStripeCustomer, callbackInner) {
            stripeCustomer.subscriptionStripeCustomer = subscriptionStripeCustomer;
            getCustomerForPayment(req.payment_customer_id, callbackInner);
        }
    ], function (err, paymentStripeCustomer) {
        if (err) {
            logger.error(err);
            callback(err);
        }
        stripeCustomer.paymentStripeCustomer = paymentStripeCustomer;
        callback(null, stripeCustomer);
    });
}


/* Customer mapping services */

function isCustomerExists(email, callback) {
    customermapping.isCustomerExists(email).then(function (response) {
        callback(null, response);
    }, function (err) {
        logger.error(err);
        callback(err);
    });
}


function getCustomer(email, callback) {
    customermapping.getCustomer(email).then(function (customer) {
        callback(null, customer);
    }, function (err) {
        logger.error(err);
        callback(err);
    });
}


function createCustomerMapping(customer, email, callback) {
    customermapping.createCustomerMapping(customer.subscription_customer_id, customer.payment_customer_id, email).then(function () {
        callback(null, customer);
    }, function (err) {
        logger.error(err);
        callback(err);
    });
}


/* End Customer mapping services */


/* Customer services for Payment stripe */

function getCustomerForPayment(customerId, callback) {
    stripePaymentService.getCustomer(customerId).then(function (customer) {
        callback(null, customer);
    }, function (err) {
        logger.error(err);
        callback(err);
    });
}


function createCustomerForPayment(email, callback) {
    stripePaymentService.createCustomer(email).then(function (customer) {
        callback(null, customer);
    }, function (err) {
        logger.error(err);
        callback(err);
    });
}

/* End Customer services for paymanet stripe */


/* Customer services for subscription stripe  */

function getCustomerForSubscription(customerId, callback) {
    stripeSubscriptionService.getCustomer(customerId).then(function (customer) {
        callback(null, customer);
    }, function (err) {
        logger.error(err);
        callback(err);
    });
}

function createCustomerForSubscription(email, callback) {
    stripeSubscriptionService.createCustomer(email).then(function (customer) {
        callback(null, customer);
    }, function (err) {
        logger.error(err);
        callback(err);
    });
}

/* End services for subscription */


module.exports = customerFilter;