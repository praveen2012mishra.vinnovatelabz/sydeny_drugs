let authorizationService = require('../services/authorizationService.js');

let tokenFilter = {};

tokenFilter.validator = function(req, res, next) {
    let token = req.header('auth-token');
    if(req.originalUrl.includes('/favicon.ico') ||
        req.originalUrl.includes('/user/validatetoken') ||
        req.originalUrl.includes('/webjars/') ||
        req.originalUrl.includes('/index.html')){
        return next();
    }else if(token){
        authorizationService.validateToken(req, token).then(function (profile) {
            if(profile){
                req.currentUser = profile;
                req.authToken = token;
                return next();
            }else{
                res.status(401).send('UNAUTHORIZED');
            }
        },function (error) {
            res.status(401).send('UNAUTHORIZED');
        });
    }else{
        res.status(401).send('UNAUTHORIZED');
    }
};

module.exports = tokenFilter;
