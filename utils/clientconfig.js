module.exports = {
    local: {
        auth0: {
            clientId: "nBOUyrKQpuHbCq044tqUS1As2I76PKdm",
            domain: "sydneydrugsauth0.au.auth0.com",
            logo: "https://d26ylqsdsxp8yv.cloudfront.net/SDIcon.png"
        },
        zendesk:{
            host : "yeshelp.zendesk.com"
        },
        intercom:{
            userIdentityKey: 'YSs__ApJjeZWZFtlHwWIiUIQ0EE5DxoU7TcmV7PE'
        },
        stripe : {
            stripe_Publishable_key : 'pk_test_i7MYsa3Sj1JCEL4x0T8o2XiG',
        }
    },
    development: {
        auth0: {
            clientId: process.env.auth0ClientId,
            domain: process.env.auth0Domain,
            logo: "https://d26ylqsdsxp8yv.cloudfront.net/SDIcon.png"
        },
        zendesk:{
            host : "sydneydrugs.zendesk.com"
        },
        intercom:{
            userIdentityKey: 'YSs__ApJjeZWZFtlHwWIiUIQ0EE5DxoU7TcmV7PE'
        },
        stripe : {
            stripe_Publishable_key : process.env.stripe_Publishable_key,
        }
    },
    production: {
    }
};