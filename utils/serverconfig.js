module.exports = {
    "local": {
        "auth0": {
            "clientId": "nBOUyrKQpuHbCq044tqUS1As2I76PKdm",
            "domain": "sydneydrugsauth0.au.auth0.com",
            "logo": "https://d26ylqsdsxp8yv.cloudfront.net/SDIcon.png"
        },
        "mysql":{
            "host":"localhost",
            "port":"3306",
            "un":"root",
            "pass":"root",
            "database":"sydneydrugs"
        },
        "stripe" : {
            "stripe_secret_key" : "sk_test_qTmoarhUxtK221jEaTv1lIZB",
            "connectedAccId" : "acct_1BVEXzGEb6glGf6Y"
        }
    },
    "development": {
        "auth0": {
            "clientId": process.env.auth0ClientId,
            "domain": process.env.auth0Domain,
            "logo": "https://d26ylqsdsxp8yv.cloudfront.net/SDIcon.png"
        },
        "mysql":{
            "host":process.env.host,
            "port":process.env.port,
            "un":process.env.username,
            "pass":process.env.password,
            "database":process.env.database
        },
        "stripe" : {
            "stripe_secret_key" : process.env.stripe_secret_key,
            "connectedAccId" : process.env.connectedAccId
        }
    },
    "production": {
    }
};